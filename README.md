# BORKS
Multifunctional task tracker.

## Description
The goal of this project is to create the app to help managing daily tasks. The repository conctains 3 projects:  

    + tracker_lib -- the core of the tracker. Includes all nessesary logic for working with tasks.
    + borks -- console app based on tracker_lib. Managing tasks using console commands and store information in json files.
    + web_borks -- web application, made with django framework.
The tracker have wide custom feauters. You're not limited by amount of priorities, types and statuses. Now you can create you own characteristics for tasks. This approach is convenient not only for personal usare, but especially for working on tasks with teams (for example some agile methodology). So use it in way you want, enjoy!
### How to install? 
#### Tracker library
#####From the bitbucket.org
Clone the project from the git repository:
~~~
git clone https://kseniya_borokhovskaya@bitbucket.org/kseniya_borokhovskaya/tasktracker.git
~~~
In cloned project go to folder 'tracker_lib':
~~~
cd tracker_lib
~~~
Install the app using this command:
~~~
python3 setup.py install
~~~
_Don't forget to install jsonpickle if you're using json storage_

#### Borks console application
First of all you need install tracker library. Use instractions from previous section. Then follow instractions written further:
In cloned project go to folder 'borks':
~~~
cd borks
~~~
Install the app using this command:
~~~
python3 setup.py install
~~~

### How to use?
####Library
~~~
>>> import os
>>> from tracker_core.data_connectors.json_storage import JsonStorage
>>> from tracker_core.manager.data_manager import DataManager

>>> path = os.path.expanduser('~/.Borks')
>>> storage = JsonStorage(path, path)  # first parametr for storing users, second for other data.
>>> manager = DataManager(storage)

>>> from tracker_core.models.user import User

>>> user = User(namespase.name, namespase.login, namespase.email)
>>> saved_user = storage.add_user(user)

>>> from tracker_core.models.task import Type, Status, Priority, Task

>>> event = manager.create_type(Type('event')) 
>>> important = manager.create_priority(Type('important'))
>>> preprocessing = manager.create_type(Type('preprocessing'))

>>>task = Task("I'm cool task!", saved_user.id,
                    type=event,
                    priority=important,
                    status=preprocessing,
                    date='2018-07-14',
                    description='Hey! Your first task is here!')
>>>manager.create_task(task)
~~~ 
###Borks console app
Use command 'borks' to start working with the app:
~~~
borks [-h] {login,sign_up,log_out,task,list,plan, reminder, priority, status, type, tag} ... 

user@user: path$ borks type create "task"
Type 2: task was saved to data base

user@user: path$ borks priority create "important" -d
Priority 1: important was saved to data base

user@user: path$ borks task create "Fix bugs in lab" -p important -t task
Task 1: Fix bugs in lab was saved to data base

user:user: path$ borks task show -t 1

Task Id: 1
Name:Fix bugs in lab
Creation date: 2018-06-25 19:27:26.683913
Type: task
Priority: important
~~~

#####_Author: Borokhovskaya Kseniya_
