import os

from tracker_core.data_connectors.json_storage import JsonStorage
from tracker_core.manager.data_manager import DataManager
from borks_tracker.authenticator import load_settings_from_config
from borks_tracker.configuration import APP_PATH
from borks_tracker.configuration import get_current_user
from borks_tracker.handlers import Handler
from borks_tracker.parsers.main_parser import Parser


def main():
    if not os.path.exists(os.path.expanduser(APP_PATH)):
        os.mkdir(os.path.expanduser(APP_PATH))

    storage = JsonStorage(os.path.expanduser(APP_PATH))

    user_id = get_current_user()

    load_settings_from_config(user_id, storage)

    manager = DataManager(storage)

    parser = Parser(manager, user_id)
    arg_parser = parser.get_arg_parser()

    namespace = arg_parser.parse_args()

    handler = Handler(manager, namespace, user_id)
    try:
        handler.perform_action()
    except Exception as e:
        print(str(e))
        return


if __name__ == "__main__":
    main()
