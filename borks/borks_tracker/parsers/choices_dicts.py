from borks_tracker.handlers import catch_exceptions


@catch_exceptions
def get_type_dict(manager, user_id):
    types = manager.get_all_types(user_id)
    type_dict = {task_type.value: task_type for task_type in types}
    return type_dict


@catch_exceptions
def get_priority_dict(manager, user_id):
    priorities = manager.get_all_priorities(user_id)
    priority_dict = {priority.value: priority for priority in priorities}
    return priority_dict


@catch_exceptions
def get_status_dict(manager, user_id):
    statuses = manager.get_all_statuses(user_id)
    status_dict = {status.value: status for status in statuses}
    return status_dict


@catch_exceptions
def get_tag_dict(manager, user_id):
    tags = manager.get_all_tags(user_id)
    tag_dict = {tag.value: tag for tag in tags}
    return tag_dict
