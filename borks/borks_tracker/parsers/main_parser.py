import argparse
import sys
from datetime import datetime

from borks_tracker.authenticator import get_logger_levels
from borks_tracker.parsers.choices_dicts import (
    get_type_dict,
    get_priority_dict,
    get_status_dict,
    get_tag_dict
)


class Parser(argparse.ArgumentParser):
    def __init__(self, manager, user_id):
        super().__init__(self)
        self.manager = manager
        self.user_id = user_id

    def error(self, message):
        sys.stderr.write('error: {}\n'.format(message))
        self.print_help()
        sys.exit(2)

    def get_arg_parser(self):
        parser = argparse.ArgumentParser(prog="TaskTracker", description="Your helper for managing you daily tasks")
        subparsers = parser.add_subparsers(help="Sing up or log in to start use task tracker", dest="command")
        subparsers.required = True

        self.create_parser_for_authentication(subparsers)
        self.create_task_parser(subparsers)
        self.create_list_parser(subparsers)
        self.create_plan_parser(subparsers)
        self.create_reminder_parser(subparsers)
        self.create_type_parser(subparsers)
        self.create_priority_parser(subparsers)
        self.create_status_parser(subparsers)
        self.create_tag_parser(subparsers)

        return parser

    def create_parser_for_authentication(self, subparsers):
        log_in_parser = subparsers.add_parser("login", help="Log in to start using task tracker")
        sign_up_parser = subparsers.add_parser("sign_up", help="Sing up to start using task tracker")
        log_out_parser = subparsers.add_parser("log_out", help="Log out to stop using task tracker")

        log_in_parser.add_argument("login", type=str, help="Enter your login")

        sign_up_parser.add_argument("name", type=str, help="Enter your name")
        sign_up_parser.add_argument("email", type=str, help="Enter your email")
        sign_up_parser.add_argument("login", type=str, help="Enter your login")
        sign_up_parser.add_argument("data_path", type=str, help="Enter path you want to store data about your tasks")
        sign_up_parser.add_argument("-f", "--file_logging", choices=get_logger_levels(),
                                    help="Enter way you want to logger work")
        sign_up_parser.add_argument("-c", "--console_logging", choices=get_logger_levels(),
                                    help="Enter way you want to logger work")
        sign_up_parser.add_argument("-p", "--logging_path", type=str,
                                    help="Enter path for logging file, otherwise path will be the same with data path")

    def create_task_parser(self, subparsers):
        task_parser = subparsers.add_parser("task", help="Perform some actions with tasks")

        task_sub_parsers = task_parser.add_subparsers(help="Choose action you want to perform",
                                                      dest="action", title="Action")
        task_sub_parsers.required = True

        self.task_create_parser(task_sub_parsers)
        self.task_add_parser(task_sub_parsers)
        self.task_delete_parser(task_sub_parsers)
        self.task_change_parser(task_sub_parsers)
        self.task_show_parser(task_sub_parsers)

    def create_list_parser(self, subparsers):
        list_parser = subparsers.add_parser("list", help="Perform actions with lists")

        list_sub_parsers = list_parser.add_subparsers(help="Choose action you want to perform",
                                                      dest="action", title="Action")
        list_sub_parsers.required = True

        self.list_create_parser(list_sub_parsers)
        self.list_add_parser(list_sub_parsers)
        self.list_delete_parser(list_sub_parsers)
        self.list_change_parser(list_sub_parsers)
        self.list_show_parser(list_sub_parsers)

    def create_plan_parser(self, subparsers):
        plan_parser = subparsers.add_parser("plan", help="Perform some actions with plans")

        plan_sub_parsers = plan_parser.add_subparsers(help="Choose action you want to perform",
                                                      dest="action", title="Action")
        plan_sub_parsers.required = True

        self.plan_create_parser(plan_sub_parsers)
        self.plan_delete_parser(plan_sub_parsers)
        self.plan_change_parser(plan_sub_parsers)
        self.plan_show_parser(plan_sub_parsers)

    def create_reminder_parser(self, subparsers):
        reminder_parser = subparsers.add_parser("reminder", help="Perform actions with reminder")

        reminder_sub_parsers = reminder_parser.add_subparsers(help="Choose action you want to perform",
                                                              dest="action", title="Action")
        reminder_sub_parsers.required = True
        self.reminder_create_parser(reminder_sub_parsers)
        # self.add_reminder_parser(reminder_sub_parsers)
        self.reminder_delete_parser(reminder_sub_parsers)
        # ListParse.change_reminder_parser(reminder_sub_parsers)
        # ListParse.show_reminder_parser(reminder_sub_parsers)

    def create_priority_parser(self, subparsers):
        priority_parser = subparsers.add_parser("priority", help="Perform some actions with priorities")

        priority_sub_parsers = priority_parser.add_subparsers(help="Choose action you want to perform",
                                                              dest="action", title="Action")
        priority_sub_parsers.required = True

        self.priority_create_parser(priority_sub_parsers)
        self.priority_delete_parser(priority_sub_parsers)
        self.priority_change_parser(priority_sub_parsers)
        self.priority_show_parser(priority_sub_parsers)

    def create_type_parser(self, subparsers):
        type_parser = subparsers.add_parser("type", help="Perform actions with types")

        type_sub_parsers = type_parser.add_subparsers(help="Choose action you want to perform",
                                                      dest="action", title="Action")
        type_sub_parsers.required = True

        self.type_create_parser(type_sub_parsers)
        self.type_delete_parser(type_sub_parsers)
        self.type_change_parser(type_sub_parsers)
        self.type_show_parser(type_sub_parsers)

    def create_status_parser(self, subparsers):
        status_parser = subparsers.add_parser("status", help="Perform some actions with statuses")

        status_sub_parsers = status_parser.add_subparsers(help="Choose action you want to perform",
                                                          dest="action", title="Action")
        status_sub_parsers.required = True

        self.status_create_parser(status_sub_parsers)
        self.status_delete_parser(status_sub_parsers)
        self.status_change_parser(status_sub_parsers)
        self.status_show_parser(status_sub_parsers)

    def create_tag_parser(self, subparsers):
        tag_parser = subparsers.add_parser("tag", help="Perform actions with tags")

        tag_sub_parsers = tag_parser.add_subparsers(help="Choose action you want to perform",
                                                    dest="action", title="Action")
        tag_sub_parsers.required = True

        self.tag_create_parser(tag_sub_parsers)
        self.tag_delete_parser(tag_sub_parsers)
        self.tag_change_parser(tag_sub_parsers)
        self.tag_show_parser(tag_sub_parsers)

    def task_create_parser(self, task_sub_parsers):
        create_parser = task_sub_parsers.add_parser('create', help="Creates task with parameters")

        create_parser.add_argument("name", type=str, help="Name of new task")
        create_parser.add_argument("-y", "--type", choices=get_type_dict(self.manager, self.user_id),
                                   help="Choose task type id. If you will not, the default type will be set")
        create_parser.add_argument("-p", "--priority", choices=get_priority_dict(self.manager, self.user_id),
                                   help="Choose task priority id. If you will not, the default priority will be set")
        create_parser.add_argument("-u", "--status", choices=get_status_dict(self.manager, self.user_id),
                                   help="Choose task status id. If you will not, the default status will be set")
        create_parser.add_argument("-d", "--date", type=self.valid_date, help="Enter date you want set task on")
        create_parser.add_argument("-s", "--start_time", type=self.valid_time, help="Enter time of starting")
        create_parser.add_argument("-f", "--finish_time", type=self.valid_time, help="Enter time of finishing")
        create_parser.add_argument("-i", "--duration", type=str,
                                   help="Enter duration")
        create_parser.add_argument("-l", "--plan", type=int,
                                   help="Choose task plan id. If you will not, the default plan will be set")
        create_parser.add_argument("-r", "--reminder", type=int,
                                   help="Choose task reminder id. If you will not, the default reminder will be set")
        create_parser.add_argument("-c", "--description", type=str,
                                   help="Write description if needed")
        create_parser.add_argument("-g", "--tag", type=list, choices=get_tag_dict(self.manager, self.user_id),
                                   nargs='+', help="Choose tags you want to mark task")
        create_parser.add_argument("-b", "--blocking", nargs='+',
                                   help="Choose tasks you want to be done first before you can do this task")
        create_parser.add_argument("-a", "--ancestor", type=int,
                                   help="Choose task id, which you want to be the ancestor of new task")

    def task_delete_parser(self, task_sub_parsers):
        delete_parser = task_sub_parsers.add_parser('delete', help="Delete task or its parameters")

        delete_parser.add_argument("task_id", type=int,
                                   help="Enter id of task you want to perform delete operation with")
        delete_parser.add_argument("-d", "--date", type=str, help="Delete date")
        delete_parser.add_argument("-s", "--start_time", action='store_true',
                                   help="Delete start time of task")
        delete_parser.add_argument("-f", "--finish_time", action='store_true',
                                   help="Delete finish time of task")
        delete_parser.add_argument("-i", "--duration", action='store_true',
                                   help="Delete duration of task")
        delete_parser.add_argument("-y", "--type", action='store_true',
                                   help="Delete type of task")
        delete_parser.add_argument("-p", "--priority", action='store_true',
                                   help="Delete priority of task")
        delete_parser.add_argument("-u", "--status", action='store_true',
                                   help="Delete status of task")
        delete_parser.add_argument("-l", "--plan", action='store_true',
                                   help="Delete plan of task")
        delete_parser.add_argument("-r", "--reminder", action='store_true',
                                   help="Delete reminder of task")
        delete_parser.add_argument("-c", "--description", action='store_true',
                                   help="Delete description of task")
        delete_parser.add_argument("-a", "--ancestor", action='store_true',
                                   help="Delete ancestor of task")
        delete_parser.add_argument("-g", "--tags", choices=get_tag_dict(self.manager, self.user_id),
                                   nargs='+', help="Choose tags you want to not mark task")
        delete_parser.add_argument("-b", "--blocking", nargs='+',
                                   help="Choose tasks you want to unwind from")

    def task_add_parser(self, task_sub_parsers):
        add_parser=task_sub_parsers.add_parser('add', help="Add objects to task's list of specified objects")

        add_parser.add_argument("task_id", type=int,
                                help="Enter id of task you want to add something to")
        add_parser.add_argument("-g", "--tags", choices=get_tag_dict(self.manager, self.user_id),
                                nargs='+', help="Choose tags you want to mark task")
        add_parser.add_argument("-b", "--blocking", nargs='+',
                                help="Choose tasks you want to be done first before you can do this task")

    def task_change_parser(self, task_sub_parsers):
        change_parser = task_sub_parsers.add_parser('change', help="Changes task's parameters")

        change_parser.add_argument("task_id", type=int,
                                   help="Enter id of task you want to change")
        change_parser.add_argument("-n", "--name", type=str, help="New name of task")
        change_parser.add_argument("-y", "--type", choices=get_type_dict(self.manager, self.user_id),
                                   help="Choose new type")
        change_parser.add_argument("-p", "--priority", choices=get_priority_dict(self.manager, self.user_id),
                                   help="Choose new priority")
        change_parser.add_argument("-u", "--status", choices=get_status_dict(self.manager, self.user_id),
                                   help="Choose new status")
        change_parser.add_argument("-d", "--date", type=self.valid_date,
                                   help="Enter new date you want set task on")
        change_parser.add_argument("-s", "--start_time", type=self.valid_time,
                                   help="Enter new time of starting")
        change_parser.add_argument("-f", "--finish_time", type=self.valid_time,
                                   help="Enter new time of finishing")
        change_parser.add_argument("-i", "--duration", type=str,
                                   help="Enter new duration")
        change_parser.add_argument("-l", "--plan", type=int,
                                   help="Choose new plan id.")
        change_parser.add_argument("-r", "--reminder", type=int,
                                   help="Choose new reminder id")
        change_parser.add_argument("-c", "--description", type=str,
                                   help="Write new description")
        change_parser.add_argument("-a", "--ancestor", type=int,
                                   help="Choose new task id, which you want to be the ancestor of task")

    def task_show_parser(self, task_sub_parsers):
        show_parser = task_sub_parsers.add_parser('show', help="Shows information task/ tasks")

        show_parser.add_argument("-a", "--all", action="store_true",
                                 help="Show all tasks")
        show_parser.add_argument("-t", "--task_id", type=int,
                                 help="Show information about task")
        show_parser.add_argument("-f", "--task_id_full", type=int,
                                 help="Show full information about task")
        show_parser.add_argument("-p", "--priority", choices=get_priority_dict(self.manager, self.user_id),
                                 help="Show tasks with entered priority")
        show_parser.add_argument("-s", "--status", choices=get_status_dict(self.manager, self.user_id),
                                 help="Show tasks with entered status")
        show_parser.add_argument("-y", "--type",  choices=get_type_dict(self.manager, self.user_id),
                                 help="Show tasks with entered type")
        show_parser.add_argument("-g", "--tag", choices=get_tag_dict(self.manager, self.user_id),
                                 help="Show tasks with entered tag")

    def list_create_parser(self, list_sub_parsers):
        create_parser = list_sub_parsers.add_parser('create', help="Creates list with parameters")

        create_parser.add_argument("name", type=str, help="Name of new list")

    def list_delete_parser(self, list_sub_parsers):
        delete_parser = list_sub_parsers.add_parser('delete', help="Delete list or its parameters")

        delete_parser.add_argument("list_id", type=int,
                                   help="Enter id of list you want to perform delete operation with")
        delete_parser.add_argument("-a", "--ancestor", action='store_true',
                                   help="Delete ancestor of list")
        delete_parser.add_argument("-l", "--lists", nargs='+',
                                   help="Choose lists you want to remove from this list")
        delete_parser.add_argument("-t", "--tasks", nargs='+',
                                   help="Choose tasks you want to remove from this list")

    def list_add_parser(self, list_sub_parsers):
        add_parser = list_sub_parsers.add_parser('add', help="Add objects to list's list of specified objects")

        add_parser.add_argument("list_id", type=int,
                                help="Enter id of list you want to add something to")
        add_parser.add_argument("-l", "--lists", nargs='+',
                                help="Enter lists you want this list to contain")
        add_parser.add_argument("-t", "--tasks", nargs='+',
                                help="Choose tasks you want to add from this list")

    def list_change_parser(self, list_sub_parsers):
        change_parser = list_sub_parsers.add_parser('change', help="Changes list's parameters")

        change_parser.add_argument("list_id", type=int,
                                   help="Enter id of list you want to change")
        change_parser.add_argument("-n", "--name", type=str, help="New name of list")
        change_parser.add_argument("-a", "--ancestor", type=int,
                                   help="Choose new list id, which you want to be the ancestor of list")

    def list_show_parser(self, list_sub_parsers):
        show_parser = list_sub_parsers.add_parser('show', help="Shows information of list/lists")

        show_parser.add_argument("-a", "--all", action="store_true",
                                 help="Show all lists")
        show_parser.add_argument("-l", "--list_id", type=int,
                                 help="Show information about list")

    def plan_create_parser(self, plan_sub_parsers):
        create_parser = plan_sub_parsers.add_parser('create', help="Creates plan of repetition of task")

        create_parser.add_argument("-d", "--date", type=self.valid_date,
                                   help="Enter date you want set")
        create_parser.add_argument("-s", "--start_time", type=self.valid_time,
                                   help="Enter time of starting")
        create_parser.add_argument("-f", "--finish_time", type=self.valid_time,
                                   help="Enter time of finishing")

        create_parser.add_argument("-t", "--start_date", type=self.valid_date,
                                   help="Enter date of starting")
        create_parser.add_argument("-i", "--finish_date", type=self.valid_date,
                                   help="Enter date of finishing")

        create_parser.add_argument("-r", "--repetition_start_date", type=self.valid_date,
                                   help="Enter date of finishing repetition")
        create_parser.add_argument("-e", "--repetition_finish_date", type=self.valid_date,
                                   help="Enter date of finishing repetition")

        create_parser.add_argument("-n", "--number", type=int,
                                   help="Enter number of times you want task repeat")
        create_parser.add_argument("-a", "--days", type=int,
                                   help="Enter number of days you want to repeat task")
        create_parser.add_argument("-w", "--weeks", type=int,
                                   help="Enter number of weeks you want to repeat task")
        create_parser.add_argument("-m", "--months", type=int,
                                   help="Enter number of months you want to repeat task")
        create_parser.add_argument("-y", "--years", type=int,
                                   help="Enter number of years you want to repeat task")

    def plan_delete_parser(self, delete_sub_parsers):
        delete_parser = delete_sub_parsers.add_parser('delete', help="Removes plan of repetition of task")

        delete_parser.add_argument("-p", "--plan_id", type=int,
                                   help="Enter id of plan you want to delete")

        delete_parser.add_argument("-d", "--date", action='store_true',
                                   help="Delete date")
        delete_parser.add_argument("-s", "--start_time", action='store_true',
                                   help="Delete time of starting")
        delete_parser.add_argument("-f", "--finish_time", action='store_true',
                                   help="Delete time of finishing")

        delete_parser.add_argument("-t", "--start_date", action='store_true',
                                   help="Delete date of starting")
        delete_parser.add_argument("-i", "--finish_date", action='store_true',
                                   help="Delete date of finishing")

        delete_parser.add_argument("-r", "--repetition_start_date", action='store_true',
                                   help="Delete date of finishing repetition")
        delete_parser.add_argument("-e", "--repetition_finish_date", action='store_true',
                                   help="Delete date of finishing repetition")

        delete_parser.add_argument("-n", "--number", action='store_true',
                                   help="Delete number of times you want task repeat")
        delete_parser.add_argument("-a", "--days", action='store_true',
                                   help="Delete number of days you want to repeat task")
        delete_parser.add_argument("-w", "--weeks", action='store_true',
                                   help="Delete number of weeks you want to repeat task")
        delete_parser.add_argument("-m", "--months", action='store_true',
                                   help="Delete number of months you want to repeat task")
        delete_parser.add_argument("-y", "--years", action='store_true',
                                   help="Delete number of years you want to repeat task")

    def plan_change_parser(self, plan_sub_parsers):
        create_parser = plan_sub_parsers.add_parser('change', help="Changes plan of repetition of task")

        create_parser.add_argument("-p", "--plan_id", type=int,
                                   help="Enter id of plan you want to change")

        create_parser.add_argument("-d", "--date", type=self.valid_date,
                                   help="Enter new date you want set")
        create_parser.add_argument("-s", "--start_time", type=self.valid_time,
                                   help="Enter new time of starting")
        create_parser.add_argument("-f", "--finish_time", type=self.valid_time,
                                   help="Enter new time of finishing")

        create_parser.add_argument("-t", "--start_date", type=self.valid_date,
                                   help="Enter new date of starting")
        create_parser.add_argument("-i", "--finish_date", type=self.valid_date,
                                   help="Enter new date of finishing")

        create_parser.add_argument("-r", "--repetition_start_date", type=self.valid_date,
                                   help="Enter new date of finishing repetition")
        create_parser.add_argument("-e", "--repetition_finish_date", type=self.valid_date,
                                   help="Enter new date of finishing repetition")

        create_parser.add_argument("-n", "--number", type=int,
                                   help="Enter new number of times you want task repeat")
        create_parser.add_argument("-a", "--days", type=int,
                                   help="Enter new number of days you want to repeat task")
        create_parser.add_argument("-w", "--weeks", type=int,
                                   help="Enter new number of weeks you want to repeat task")
        create_parser.add_argument("-m", "--months", type=int,
                                   help="Enter new number of months you want to repeat task")
        create_parser.add_argument("-y", "--years", type=int,
                                   help="Enter new number of years you want to repeat task")

    def plan_show_parser(self, plan_sub_parsers):
        show_parser = plan_sub_parsers.add_parser('show', help="Shows information of plan/plans")

        show_parser.add_argument("-a", "--all", action="store_true",
                                 help="Show all plans")
        show_parser.add_argument("-p", "--plan_id", type=int,
                                 help="Show information about plan")

    def priority_create_parser(self, priority_sub_parsers):
        create_parser = priority_sub_parsers.add_parser('create', help="Creates priority for tasks")

        create_parser.add_argument("value", type=str, help="Enter priority you want to create")
        create_parser.add_argument("-d", "--default", action="store_true",
                                   help="Makes new priority default. Every new task will be created with this priority.")

    def priority_delete_parser(self, priority_sub_parsers):
        delete_parser = priority_sub_parsers.add_parser('delete', help="Removes priority")
        delete_parser.add_argument("priority_id", type=str, help="Enter priority you want to delete")

    def priority_change_parser(self, priority_sub_parsers):
        create_parser = priority_sub_parsers.add_parser('create', help="Creates priority for tasks")

        create_parser.add_argument("value", type=str, help="Enter new priority you want to create")
        create_parser.add_argument("-d", "--default", action="store_true",
                                   help="Makes new priority default. Every new task will be created with this priority.")

    def priority_show_parser(self, priority_sub_parsers):
        show_parser = priority_sub_parsers.add_parser('show', help="Shows information of priorities")

        show_parser.add_argument("-a", "--all", action="store_true",
                                 help="Show all priorities")
        show_parser.add_argument("-p", "--priority_id", type=int,
                                 help="Show information about priority")

    def type_create_parser(self, type_sub_parsers):
        create_parser = type_sub_parsers.add_parser('create', help="Creates type of tasks")

        create_parser.add_argument("value", type=str, help="Enter type you want to create")
        create_parser.add_argument("-d", "--default", action="store_true",
                                   help="Makes new type default. Every new task will be created with this type.")

    def type_delete_parser(self, type_sub_parsers):
        delete_parser = type_sub_parsers.add_parser('delete', help="Removes type")
        delete_parser.add_argument("type_id", type=str, help="Enter type you want to delete")

    def type_change_parser(self, type_sub_parsers):
        create_parser = type_sub_parsers.add_parser('create', help="Creates type for tasks")

        create_parser.add_argument("value", type=str, help="Enter new type you want to create")
        create_parser.add_argument("-d", "--default", action="store_true",
                                   help="Makes new type default. Every new task will be created with this type.")

    def type_show_parser(self, type_sub_parsers):
        show_parser = type_sub_parsers.add_parser('show', help="Shows information of priorities")

        show_parser.add_argument("-a", "--all", action="store_true",
                                 help="Show all priorities")
        show_parser.add_argument("-p", "--type_id", type=int,
                                 help="Show information about type")

    def status_create_parser(self, status_sub_parsers):
        create_parser = status_sub_parsers.add_parser('create', help="Creates status of tasks")

        create_parser.add_argument("value", type=str, help="Enter status you want to create")
        create_parser.add_argument("-d", "--default", action="store_true",
                                   help="Makes new status default. Every new task will be created with this status.")

    def status_delete_parser(self, status_sub_parsers):
        delete_parser = status_sub_parsers.add_parser('delete', help="Removes status")
        delete_parser.add_argument("status_id", type=str, help="Enter status you want to delete")

    def status_change_parser(self, status_sub_parsers):
        create_parser = status_sub_parsers.add_parser('create', help="Creates status for tasks")

        create_parser.add_argument("value", type=str, help="Enter new status you want to create")
        create_parser.add_argument("-d", "--default", action="store_true",
                                   help="Makes new status default. Every new task will be created with this status.")

    def status_show_parser(self, status_sub_parsers):
        show_parser = status_sub_parsers.add_parser('show', help="Shows information of priorities")

        show_parser.add_argument("-a", "--all", action="store_true",
                                 help="Show all priorities")
        show_parser.add_argument("-p", "--status_id", type=int,
                                 help="Show information about status")

    def tag_create_parser(self, tag_sub_parsers):
        create_parser = tag_sub_parsers.add_parser('create', help="Creates tag of tasks")

        create_parser.add_argument("value", type=str, help="Enter tag you want to create")
        create_parser.add_argument("-d", "--default", action="store_true",
                                   help="Makes new tag default. Every new task will be created with this tag.")

    def tag_delete_parser(self, tag_sub_parsers):
        delete_parser = tag_sub_parsers.add_parser('delete', help="Removes tag")
        delete_parser.add_argument("tag_id", type=str, help="Enter tag you want to delete")

    def tag_change_parser(self, tag_sub_parsers):
        create_parser = tag_sub_parsers.add_parser('create', help="Creates tag for tasks")

        create_parser.add_argument("value", type=str, help="Enter new tag you want to create")
        create_parser.add_argument("-d", "--default", action="store_true",
                                   help="Makes new tag default. Every new task will be created with this tag.")

    def tag_show_parser(self, tag_sub_parsers):
        show_parser = tag_sub_parsers.add_parser('show', help="Shows information of priorities")

        show_parser.add_argument("-a", "--all", action="store_true",
                                 help="Show all priorities")
        show_parser.add_argument("-p", "--tag_id", type=int,
                                 help="Show information about tag")

    def reminder_delete_parser(self, reminder_sub_parsers):
        delete_reminder_parser = reminder_sub_parsers.add_parser('delete', help="Removes reminders for task")

        delete_reminder_parser.add_argument("-s", "--is-on-start-date", type=bool,
                                            help="Enter true if you want to get reminder on date of start")
        delete_reminder_parser.add_argument("-f", "--is-on-finish-date", type=bool,
                                            help="Enter true if you want to get reminder on date of finish")
        delete_reminder_parser.add_argument("-d", "--dates-of-reminder", type=list,
                                            help="Enter dates you want to get reminders")
        delete_reminder_parser.add_argument("-n", "--number", type=str, help="Enter number of periods")
        delete_reminder_parser.add_argument("-p", "--kind-of-period", type=str,
                                            help="Enter kind of repetition period")

    def reminder_create_parser(self, reminder_sub_parsers):
        add_reminder_parser = reminder_sub_parsers.add_parser('create', help="Creates reminders for task")

        add_reminder_parser.add_argument("-s", "--is-on-start-date", type=bool,
                                         help="Enter true if you want to get reminder on date of start")
        add_reminder_parser.add_argument("-f", "--is-on-finish-date", type=bool,
                                         help="Enter true if you want to get reminder on date of finish")
        add_reminder_parser.add_argument("-d", "--dates-of-reminder", type=list,
                                         help="Enter dates you want to get reminders")
        add_reminder_parser.add_argument("-n", "--number", type=str, help="Enter number of periods")
        add_reminder_parser.add_argument("-p", "--kind-of-period", type=str,
                                         help="Enter kind of repetition period")

    def valid_date(self, date):
        try:
            valid_date = (datetime.strptime(date, "%Y-%m-%d").date()
                          or datetime.strptime(date, "%Y-%m-%d %H:%M").date())
        except ValueError:
            raise argparse.ArgumentTypeError("Not a valid date: '{}'.".format(date))

        if valid_date < datetime.now().date():
            raise argparse.ArgumentTypeError("Date {} in the past".format(valid_date))

    def valid_time(self, time):
        try:
            return datetime.strptime(time, "%H:%M").time()
        except ValueError:
            raise argparse.ArgumentTypeError("Not a valid time: {}.".format(time))
