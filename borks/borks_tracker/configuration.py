import configparser
import os
# from terminal_tracker.task_tracker.authenticator import LOGGING_PATH, CONSOLE_LOGGING, FILE_LOGGING

LOGGING_PATH = "logging_path"
FILE_LOGGING = "file_logging"
CONSOLE_LOGGING = "console_logging"

DATA = "DATA"
USER = "user"
DATA_PATH = "data_path"
LOGGING = "LOGGING"

APP_PATH = '~/.Borks'
CONFIG_PATH = APP_PATH + '/config'


def set_configuration(user_id, data_path=None, logging_path=None, logging_file_type=None, logging_console_type=None):
    config_path = os.path.join(os.path.expanduser(APP_PATH), 'config_{}'.format(user_id))

    if data_path is None:
        data_path = APP_PATH
    if logging_path is None:
        logging_path = APP_PATH
    if logging_console_type is None:
        logging_console_type = "info"
    if logging_file_type is None:
        logging_file_type = "debug"

    config = configparser.RawConfigParser()
    config[DATA] = {}
    config[DATA][USER] = str(user_id)
    config[DATA][DATA_PATH] = data_path
    config[LOGGING] = {}
    config[LOGGING][LOGGING_PATH] = logging_path
    config[LOGGING][FILE_LOGGING] = logging_file_type
    config[LOGGING][CONSOLE_LOGGING] = logging_console_type

    with open(config_path, 'w') as config_file:
        config.write(config_file)


def get_current_user():
    if os.path.isfile(os.path.expanduser(CONFIG_PATH)) and os.path.getsize(os.path.expanduser(CONFIG_PATH)) > 0:
        with open(os.path.expanduser(CONFIG_PATH), "r") as config:
            return config.read()
    else:
        return None


def set_current_user(user_id = None):
    with open(os.path.expanduser(CONFIG_PATH), "w") as config:
        config.write(str(user_id))

