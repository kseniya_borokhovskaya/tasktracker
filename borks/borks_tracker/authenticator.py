import configparser
import logging
import os

from tracker_core.logging import (
    get_logger
)
from tracker_core.models.user import User
from borks_tracker.configuration import (
    APP_PATH,
    DATA_PATH,
    LOGGING,
    DATA,
    set_configuration,
    set_current_user
)

LOGGING_PATH_CONFIG_FIELD = "logging_path"
FILE_LOGGING_CONFIG_FIELD = "file_logging"
CONSOLE_LOGGING_CONFIG_FIELD = "console_logging"


def handle_authentication(storage, namespase):
    handlers = get_authentication_handlers()
    command = namespase.command

    if command in handlers:
        handlers[command](storage, namespase)


def get_authentication_handlers():
    return {
        "login": login,
        "log_out": log_out,
        "sign_up": sign_up
    }


def login(storage, namespase):
    user = storage.get_user_by_login(namespase.login)

    load_settings_from_config(user.id, storage)

    set_current_user(user.id)

    return user


def log_out(storage, namespase):
    set_current_user()


def sign_up(storage, namespase):
    new_user = User(namespase.name, namespase.login, namespase.email)
    user = storage.add_user(new_user)

    set_configuration(user.id, namespase.data_path, namespase.logging_path,
                      namespase.file_logging, namespase.console_logging)

    set_current_user(user.id)

    load_settings_from_config(user.id, storage)

    return user


def load_settings_from_config(user_id, storage):
    config_path = os.path.join(os.path.expanduser(APP_PATH), 'config_{}'.format(user_id))
    if os.path.exists(config_path):
        config = configparser.RawConfigParser()
        config.read(config_path)

        set_logger_settings(config[LOGGING])

        storage.reload_data_path(config[DATA][DATA_PATH])


def set_logger_settings(settings):
    logger = get_logger()

    levels = get_logger_levels()

    if LOGGING_PATH_CONFIG_FIELD in settings and FILE_LOGGING_CONFIG_FIELD in settings:
        file_handler = logging.FileHandler(os.path.expanduser(settings[LOGGING_PATH_CONFIG_FIELD]) + '/logs')
        logger.setLevel(levels[settings[FILE_LOGGING_CONFIG_FIELD]])
    else:
        file_handler = logging.FileHandler(os.path.expanduser(APP_PATH) + '/logs')
        logger.setLevel(logging.DEBUG)

    logger.addHandler(file_handler)

    console_handler = logging.StreamHandler()
    if CONSOLE_LOGGING_CONFIG_FIELD in settings:
        logger.setLevel(levels[settings[CONSOLE_LOGGING_CONFIG_FIELD]])
    else:
        logger.setLevel(logging.INFO)

    logger.addHandler(console_handler)


def get_logger_levels():
    """Returns possible logger levels."""
    return {
        "info": logging.INFO,
        "debug": logging.DEBUG,
        "warning": logging.WARNING,
        "error": logging.ERROR,
        "none": logging.NOTSET
    }