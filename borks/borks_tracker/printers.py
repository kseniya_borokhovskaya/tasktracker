
def print_task_info(task):
    print('\nTask Id: {0.id}\nName:{0.name}\nCreation date: {0.creation_date}'.format(task))

    if task.status is not None:
        print("Status: {}\n".format(task.status.value))

    if task.priority is not None:
        print("Priority: {}\n".format(task.priority.value))

    if task.type is not None:
        print("Type: {}\n".format(task.type.value))

    if task.description != "" and task.description is not None:
        print("Description: {}\n".format(task.description))

    if task.tags is not None:
        print("Tags: ")
        for tag in task.tags:
            print(tag.value + " ")
        print("\n")

    print('-----------------------------------------------------------\n')


def print_plan_info(plan):
    print('\nPlan Id: {0.id}\n'.format(plan))

    if plan.time.date is not None:
        print("Date: {}\n".format(plan.time.date))

    if plan.time.start_time is not None:
        print("Start time: {}\n".format(plan.time.start_time))

    if plan.time.finish_time is not None:
        print("Finish time: {}\n".format(plan.time.finish_time))

    if plan.start_date is not None:
        print("Start date: {}\n".format(plan.start_date))

    if plan.finish_date is not None:
        print("Finish date: {}\n".format(plan.finish_date))

    if plan.repetition_start_date is not None:
        print("Repetition start date: {}\n".format(plan.repetition_start_date))

    if plan.repetition_finish_date is not None:
        print("Repetition finish date: {}\n".format(plan.repetition_finish_date))

    if plan.repetition.number_of_periods is not None:
        print("Number of periods: {}\n".format(plan.repetition.number_of_periods))

    if plan.repetition.days_of_week is not None:
        print("Days of week: {}\n".format(plan.repetition.days_of_week))

    if plan.repetition.year is not None:
        print("Years: {}\n".format(plan.repetition.year))

    if plan.repetition.month is not None:
        print("Months: {}\n".format(plan.repetition.month))

    if plan.repetition.week is not None:
        print("Weeks: {}\n".format(plan.repetition.week))

    if plan.repetition.day is not None:
        print("Days: {}\n".format(plan.repetition.day))

    print('-----------------------------------------------------------\n')


def print_task_tree(storage, task, indent):
    tree = ""
    tree += "{0}+ {1}".format(indent, task.name) + "\n"

    for task in storage.get_task_sub_tasks(task.id):
        tree += print_task_tree(storage, task, indent + "\t")

    return tree


def print_list_info(list):
    print('\nList Id: {0.id}\nName:{0.name}\n'.format(list))
    print('-----------------------------------------------------------\n')


def print_priority_info(priority):
    print('\nPriority Id: {0.id}\nName:{0.value}\n\tDefault: {0.default}'.format(priority))
    print('-----------------------------------------------------------\n')


def print_status_info(status):
    print('\nSatus Id: {0.id}\nName:{0.value}\n\tDefault: {0.default}'.format(status))
    print('-----------------------------------------------------------\n')


def print_type_info(type):
    print('\nType Id: {0.id}\nName:{0.value}\n\tDefault: {0.default}'.format(type))
    print('-----------------------------------------------------------\n')


def print_tag_info(tag):
    print('\nTag Id: {0.id}\nName:{0.value}\n\tDefault: {0.default}'.format(tag))
    print('-----------------------------------------------------------\n')
