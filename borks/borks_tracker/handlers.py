from tracker_core.models.list import List
from tracker_core.models.task import (
    Task,
    Priority,
    Type,
    Status,
    Tag
)
from tracker_core.models.schedule import (
    Plan,
    Time,
    Kind
)
from borks_tracker.authenticator import handle_authentication
from borks_tracker.printers import (
    print_task_info,
    print_list_info,
    print_priority_info,
    print_status_info,
    print_tag_info,
    print_type_info,
    print_plan_info
)


CREATE_ACTION_NAME = "create"
DELETE_ACTION_NAME = "delete"
ADD_ACTION_NAME = "add"
CHANGE_ACTION_NAME = "change"
SHOW_ACTION_NAME = "show"


def catch_exceptions(func):
    def wrapper(handler, *args, **kwargs):
        try:
            return func(handler, *args, **kwargs)
        except Exception as e:
            print(str(e))
    return wrapper


class Handler:
    def __init__(self, manager, namespace, user_id):
        self.manager = manager
        self.namespace = namespace
        self.user_id = user_id

    def get_object_functions(self):
        return {
            "task": self.get_functions_for_task,
            "list": self.get_functions_for_list,
            "plan": self.get_functions_for_plan,
            "reminder": self.get_functions_for_reminder,
            "priority": self.get_functions_for_priority,
            "status": self.get_functions_for_status,
            "type": self.get_functions_for_type,
            "tag": self.get_functions_for_tag
        }

    def get_functions_for_task(self):
        return {
            CREATE_ACTION_NAME: self.create_task,
            DELETE_ACTION_NAME: self.delete_task,
            ADD_ACTION_NAME: self.update_task,
            CHANGE_ACTION_NAME: self.update_task,
            SHOW_ACTION_NAME: self.show_task
        }

    def get_functions_for_list(self):
        return {
            CREATE_ACTION_NAME: self.create_list,
            DELETE_ACTION_NAME: self.delete_list,
            ADD_ACTION_NAME: self.update_list,
            CHANGE_ACTION_NAME: self.update_list,
            SHOW_ACTION_NAME: self.show_list
        }

    def get_functions_for_plan(self):
        return {
            CREATE_ACTION_NAME: self.create_plan,
            DELETE_ACTION_NAME: self.delete_plan,
            ADD_ACTION_NAME: self.update_plan,
            CHANGE_ACTION_NAME: self.update_plan,
            SHOW_ACTION_NAME: self.show_plan
        }

    def get_functions_for_reminder(self):
        return {
            CREATE_ACTION_NAME: self.create_reminder,
            DELETE_ACTION_NAME: self.delete_reminder,
            ADD_ACTION_NAME: self.add_reminder,
            CHANGE_ACTION_NAME: self.change_reminder,
            SHOW_ACTION_NAME: self.show_reminder
        }

    def get_functions_for_priority(self):
        return {
            CREATE_ACTION_NAME: self.create_priority,
            DELETE_ACTION_NAME: self.delete_priority,
            CHANGE_ACTION_NAME: self.update_priority,
            SHOW_ACTION_NAME: self.show_priority
        }

    def get_functions_for_status(self):
        return {
            CREATE_ACTION_NAME: self.create_status,
            DELETE_ACTION_NAME: self.delete_status,
            CHANGE_ACTION_NAME: self.update_status,
            SHOW_ACTION_NAME: self.show_status
        }

    def get_functions_for_type(self):
        return {
            CREATE_ACTION_NAME: self.create_type,
            DELETE_ACTION_NAME: self.delete_type,
            CHANGE_ACTION_NAME: self.update_type,
            SHOW_ACTION_NAME: self.show_type
        }

    def get_functions_for_tag(self):
        return {
            CREATE_ACTION_NAME: self.create_tag,
            DELETE_ACTION_NAME: self.delete_tag,
            CHANGE_ACTION_NAME: self.update_tag,
            SHOW_ACTION_NAME: self.show_tag
        }

    @catch_exceptions
    def perform_action(self):
        handle_authentication(self.manager.storage, self.namespace)

        if self.user_id is None:
            raise Exception("Can't find authenticated user, login or sing up to start using tracker")

        command = self.namespace.command
        objects_funcs_dict = self.get_object_functions()

        if command in objects_funcs_dict:
            actions_funcs_dict = objects_funcs_dict[command]()
            action = self.namespace.action
            if action in actions_funcs_dict:
                actions_funcs_dict[action]()

    def delete_obj(self, update, delete, id):
        args = vars(self.namespace)

        params = {}

        for argument in args:
            if (isinstance(args[argument], list) and len(args[argument]) != 0) or args[argument] is True:
                params.update({argument: args[argument]})

        if len(params) > 0:
            update(self.user_id, id, params)
        else:
            delete(self.user_id, id)

    def update_obj(self, update, id):
        args = vars(self.namespace)

        params = {}

        for argument in args:
            if args[argument] is not None:
                params.update({argument: args[argument]})

        update(self.user_id, id, params)

    # task
    def create_task(self):
        args = vars(self.namespace)
        task = Task(args['name'], self.user_id,
                    type=args['type'],
                    priority=args['priority'],
                    status=args['status'],
                    plan=args['plan'],
                    date=args['date'],
                    start_time=args['start_time'],
                    finish_time=args['finish_time'],
                    duration=args['duration'],
                    tags=args['tag'],
                    reminder=['reminder'],
                    description=args['description'])
        self.manager.create_task(task)

    def delete_task(self):
        self.delete_obj(self.manager.update_task, self.manager.delete_task, self.namespace.task_id)

    def update_task(self):
        self.update_obj(self.manager.update_task, self.namespace.task_id)

    def show_task(self):
        args = vars(self.namespace)
        task_id = args['task_id']
        tasks = []
        if args['all'] is True and task_id is None:
            tasks = self.manager.get_all_tasks(self.user_id)
            for task in tasks:
                print_task_info(task)
            return
        if task_id is not None:
            task = self.manager.get_task(self.user_id, task_id)
            print_task_info(task)
            return

        if args['priority'] is not None and args['status'] is not None:
            tasks = self.manager.get_filtered_tasks(
                self.user_id, lambda task: task.priority == args['priority'] and task.status == args['status'])
            for task in tasks:
                print_task_info(task)
            return

        if args['priority'] is not None and args['type'] is not None:
            tasks = self.manager.get_filtered_tasks(
                self.user_id, lambda task: task.priority == args['priority'] and task.type == args['type'])
            for task in tasks:
                print_task_info(task)
            return

        if args['priority'] is not None and args['tag'] is not None:
            tasks = self.manager.get_filtered_tasks(
                self.user_id, lambda task: task.priority == args['priority'] and task.tag == args['tag'])
            for task in tasks:
                print_task_info(task)
            return

        if args['type'] is not None and args['status'] is not None:
            tasks = self.manager.get_filtered_tasks(
                self.user_id, lambda task: task.type == args['type'] and task.status == args['status'])
            for task in tasks:
                print_task_info(task)
            return

        if args['type'] is not None and args['tag'] is not None:
            tasks = self.manager.get_filtered_tasks(
                self.user_id, lambda task: task.type == args['type'] and task.tag == args['tag'])
            for task in tasks:
                print_task_info(task)
            return

        if args['status'] is not None and args['tag'] is not None:
            tasks = self.manager.get_filtered_tasks(
                self.user_id, lambda task: task.status == args['status'] and task.tag == args['tag'])
            for task in tasks:
                print_task_info(task)
            return

        if args['priority'] is not None:
            tasks = self.manager.get_filtered_tasks(self.user_id, lambda task: task.priority == args['priority'])
            for task in tasks:
                print_task_info(task)

        if args['status'] is not None:
            tasks = self.manager.get_filtered_tasks(self.user_id, lambda task: task.status == args['status'])
            for task in tasks:
                print_task_info(task)

        if args['type'] is not None:
            tasks = self.manager.get_filtered_tasks(self.user_id, lambda task: task.type == args['type'])
            for task in tasks:
                print_task_info(task)

        if args['tag'] is not None:
            tasks = self.manager.get_filtered_tasks(self.user_id, lambda task: task.tag == args['tag'])
            for task in tasks:
                print_task_info(task)

    # list
    def create_list(self):
        list = List(self.namespace.name,
                    owner=self.user_id)
        self.manager.create_list(list)

    def delete_list(self):
        self.delete_obj(self.manager.update_list, self.manager.delete_list, self.namespace.list_id)

    def update_list(self):
        self.update_obj(self.manager.update_list, self.namespace.list_id)

    def show_list(self):
        list_id = self.namespace.list_id
        if self.namespace.all is True and list_id is None:
            lists = self.manager.get_all_lists()
            for list in lists:
                print_list_info(list)
        else:
            list = self.manager.get_list(list_id)
            print_list_info(list)

    # plan
    def create_plan(self):
        args = vars(self.namespace)

        time = Time(date=args['date'],
                    start_time=args['start_time'],
                    finish_time=args['finish_time'])

        repetition = Kind(number_of_periods=args['number'],
                          day=args['days'],
                          week=args['weeks'],
                          month=args['months'],
                          year=args['years'])

        plan = Plan(time, args['start_date'],
                    args['finish_date'],
                    args['repetition_start_date'],
                    args['repetition_finish_date'],
                    repetition)

        self.manager.create_plan(plan)

    def delete_plan(self):
        self.delete_obj(self.manager.update_paln, self.manager.delete_plan, self.namespace.plan_id)

    def update_plan(self):
        self.update_obj(self.manager.update_plan, self.namespace.plan_id)

    def show_plan(self):
        plan_id = self.namespace.plan_id
        if self.namespace.all is True and plan_id is None:
            plans = self.manager.get_all_plans()
            for plan in plans:
                print_plan_info(plan)
        else:
            plan = self.manager.get_plan(plan_id)

    def create_priority(self):
        priority = Priority(self.namespace.value, self.namespace.default)
        self.manager.create_priority(priority)

    def delete_priority(self):
        self.manager.delete_priority(self.namespace.priority_id)

    def update_priority(self):
        self.update_obj(self.manager.update_priority, self.namespace.priority_id)

    def show_priority(self):
        priority_id = self.namespace.priority_id
        if self.namespace.all is True and priority_id is None:
            priorities = self.manager.get_all_priorities()
            for priority in priorities:
                print_priority_info(priority)
        else:
            priority = self.manager.get_priority(priority_id)
            print_priority_info(priority)

    def create_type(self):
        type = Type(self.namespace.value, self.namespace.default)
        self.manager.create_type(type)

    def delete_type(self):
        self.manager.delete_type(self.namespace.type_id)

    def update_type(self):
        self.update_obj(self.manager.update_type, self.namespace.type_id)

    def show_type(self):
        type_id = self.namespace.type_id
        if self.namespace.all is True and type_id is None:
            types = self.manager.get_all_types()
            for type in types:
                print_type_info(type)
        else:
            type = self.manager.get_type(type_id)
            print_type_info(type)

    def create_status(self):
        status = Status(self.namespace.value, self.namespace.default)
        self.manager.create_status(status)

    def delete_status(self):
        self.manager.delete_status(self.namespace.status_id)

    def update_status(self):
        self.update_obj(self.manager.update_status, self.namespace.status_id)

    def show_status(self):
        status_id = self.namespace.status_id
        if self.namespace.all is True and status_id is None:
            statuses = self.manager.get_all_statuses()
            for status in statuses:
                print_status_info(status)
        else:
            status = self.manager.get_status(status_id)
            print_status_info(status)

    def create_tag(self):
        tag = Tag(self.namespace.value, self.namespace.default)
        self.manager.create_tag(tag)

    def delete_tag(self):
        self.manager.delete_tag(self.namespace.tag_id)

    def update_tag(self):
        self.update_obj(self.manager.update_tag, self.namespace.tag_id)

    def show_tag(self):
        tag_id = self.namespace.tag_id
        if self.namespace.all is True and tag_id is None:
            tags = self.manager.get_all_tags()
            for tag in tags:
                print_tag_info(tag)
        else:
            tag = self.manager.get_tag(tag_id)
            print_tag_info(tag)

    def create_reminder(self):
        return

    def delete_reminder(self):
        return

    def add_reminder(self):
        return

    def change_reminder(self):
        return

    def show_reminder(self):
        return










