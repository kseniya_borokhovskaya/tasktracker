from setuptools import setup, find_packages

setup(
    name='borks',
    author='Kseniya Borokhovskaya',
    version='1.3',
    packages=find_packages(),
    description='Command line task tracker.',
    entry_points={
          'console_scripts': [
              'borks=borks_tracker.main:main'
          ]
    }
)
