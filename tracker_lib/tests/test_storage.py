import unittest
import os
import shutil

from tracker_core.data_connectors.json_storage import (
    JsonStorage,
    DEFAULT_CORE_DIR
)
from tracker_core.exeptions import (
    ObjectNotFound,
    UserAlreadyExists,
    PermissionsError,
    InvalidDate,
    BlockedTask
)
from tracker_core.manager.data_manager import DataManager
from tracker_core.models.user import User
from tracker_core.models.task import (
    Task,
    Priority,
    Status,
    Type,
    Tag
)
from tracker_core.models.list import List
from tracker_core.models.schedule import (
    Plan,
    Time,
    Kind
)


class MyTestCase(unittest.TestCase):
    def setUp(self):
        self.path = os.path.join(os.path.abspath(os.curdir), 'tests')
        os.mkdir(os.path.expanduser(self.path))

        self.storage = JsonStorage(self.path, self.path)

        self.manager = DataManager(self.storage)

        self.user = User("Kseniya Boro", "borokhvs", "kseniya.borokhovskaya@gmail.com")
        self.storage.add_user(self.user)

        self.fake = User("Not good user", "user", "email@gmail.com")
        self.storage.add_user(self.fake)

    def tearDown(self):
        shutil.rmtree(self.path)

    def test_create_task(self):
        task1 = Task("task1", self.user.id)
        saved_task = self.manager.create_task(task1)

        self.assertEqual(saved_task.id, 1)
        self.assertEqual(saved_task.owner, self.user.id)
        self.assertEqual(saved_task.assignee, self.user.id)
        self.assertEqual(saved_task.assignee, saved_task.owner)

        self.assertIsNone(saved_task.priority)
        self.assertIsNone(saved_task.status)
        self.assertIsNone(saved_task.type)
        self.assertIsNone(saved_task.date)

        description = "Check description"
        date = "2018-07-07"
        start_time = "12:00"
        finish_time = "14:00"

        task2 = Task("task2", self.user.id,
                     description=description,
                     date=date,
                     start_time=start_time,
                     finish_time=finish_time)

        saved_task2 = self.manager.create_task(task2)
        self.assertEqual(saved_task2.id, 2)

        self.assertEqual(saved_task2.description, description)
        self.assertEqual(saved_task2.date, date)
        self.assertEqual(saved_task2.start_time, start_time)
        self.assertEqual(saved_task2.finish_time, finish_time)

        task3 = Task("task3", self.user.id,
                     start_time=finish_time,
                     finish_time=start_time)

        with self.assertRaises(InvalidDate):
            _ = self.manager.create_task(task3)

        with self.assertRaises(TypeError):
            _ = self.manager.create_task("Not a task")

    def test_delete_task(self):
        task = self.manager.create_task(Task("task4", self.user.id))
        task_id = task.id

        with self.assertRaises(PermissionsError):
            self.manager.delete_task(self.fake.id, task_id)

        with self.assertRaises(ObjectNotFound):
            self.manager.delete_task(1234, task_id)

        with self.assertRaises(ObjectNotFound):
            self.manager.delete_task(self.user.id, 1234)

        self.manager.delete_task(self.user.id, task_id)
        with self.assertRaises(ObjectNotFound):
            self.manager.get_task(self.user.id, task_id)

    def test_update_task(self):
        task = Task("task_to_update", self.user.id)
        saved_task = self.manager.create_task(task)

        name = "New name"
        another_name = "Another name"
        description = "Check description"
        date = "2018-07-07"
        start_time = "12:00"
        finish_time = "14:00"

        update_params = {
            "name": name,
            "description": description,
            "date": date,
            "start_time": start_time,
            "finish_time": finish_time,
        }

        updated_task = self.manager.update_task(self.user.id, saved_task.id, update_params)

        self.assertEqual(updated_task.id, saved_task.id)
        self.assertEqual(updated_task.name, name)
        self.assertEqual(updated_task.owner, self.user.id)
        self.assertEqual(updated_task.description, description)
        self.assertEqual(updated_task.date, date)
        self.assertEqual(updated_task.start_time, start_time)
        self.assertEqual(updated_task.finish_time, finish_time)

        update_params_2 = {
            "name": another_name,
            "description": " ",
            "date": None,
            "start_time": None,
            "finish_time": None
        }

        updated_task = self.manager.update_task(self.user.id, saved_task.id, update_params_2)

        self.assertEqual(updated_task.name, another_name)
        self.assertEqual(updated_task.owner, self.user.id)
        self.assertEqual(updated_task.description, " ")
        self.assertIsNone(updated_task.start_time)
        self.assertIsNone(updated_task.finish_time)

        invalid_params = {
            "start_time": finish_time,
            "finish_time": start_time
        }

        with self.assertRaises(InvalidDate):
            _ = self.manager.update_task(self.user.id, saved_task.id, invalid_params)

    def test_blocked_task_interaction(self):
        task = self.manager.create_task(Task("blocking_task", self.user.id))

        saved_blocked_task = self.manager.create_task(Task("blocked task", self.user.id))

        blocked_task = self.manager.update_task(self.user.id, saved_blocked_task.id, {"blocking": [task.id]})

        with self.assertRaises(BlockedTask):
            _ = self.manager.update_task(self.user.id, saved_blocked_task.id, {"name": "name"})

        with self.assertRaises(BlockedTask):
            self.manager.delete_task(self.user.id, blocked_task.id)

    def test_sub_task_interaction(self):
        task = self.manager.create_task(Task("task", self.user.id))

        first_sub_task = self.manager.create_task(Task("first sub task", self.user.id))
        second_sub_task = self.manager.create_task(Task("second sub task", self.user.id))
        third_sub_task = self.manager.create_task(Task("third sub task", self.user.id))

        _ = self.manager.update_task(self.user.id, task.id,
                                             {"sub_tasks": [first_sub_task.id, second_sub_task.id, third_sub_task.id]})

        sub_tasks_id = [task.id for task in self.manager.get_task_sub_tasks(self.user.id, task.id)]

        self.assertTrue(first_sub_task.id in sub_tasks_id)
        self.assertTrue(second_sub_task.id in sub_tasks_id)
        self.assertTrue(third_sub_task.id in sub_tasks_id)

        _ = self.manager.update_task(self.user.id, task.id, {"sub_tasks": [third_sub_task.id]})

        new_sub_tasks_id = [task.id for task in self.manager.get_task_sub_tasks(self.user.id, task.id)]

        self.assertTrue(first_sub_task.id in new_sub_tasks_id)
        self.assertTrue(second_sub_task.id in new_sub_tasks_id)
        self.assertFalse(third_sub_task.id in new_sub_tasks_id)

    def test_create_list(self):
        list1 = List("list1", self.user.id)
        saved_list = self.manager.create_list(list1)

        self.assertEqual(saved_list.id, 1)
        self.assertEqual(saved_list.owner, self.user.id)

        self.assertIsNone(saved_list.isListOfTasks)

        list2 = List("list2", self.user.id)

        saved_list2 = self.manager.create_list(list2)
        self.assertEqual(saved_list2.id, 2)
        self.assertEqual(saved_list2.owner, self.user.id)

        with self.assertRaises(TypeError):
            _ = self.manager.create_list("Not a list")

    def test_delete_list(self):
        list = self.manager.create_list(List("list", self.user.id))
        list_id = list.id

        with self.assertRaises(PermissionsError):
            self.manager.delete_list(self.fake.id, list_id)

        with self.assertRaises(ObjectNotFound):
            self.manager.delete_list(1234, list_id)

        with self.assertRaises(ObjectNotFound):
            self.manager.delete_list(self.user.id, 1234)

        self.manager.delete_list(self.user.id, list_id)
        with self.assertRaises(ObjectNotFound):
            self.manager.get_list(self.user.id, list_id)

    def test_update_list(self):

        list1 = List("list1", self.user.id)
        saved_list = self.manager.create_list(list1)

        name = "New name"
        another_name = "Another new name"

        updated_list = self.manager.update_list(self.user.id, saved_list.id, {"name": name})

        self.assertEqual(updated_list.id, saved_list.id)
        self.assertEqual(updated_list.name, name)
        self.assertEqual(updated_list.owner, self.user.id)

        newly_updated_list = self.manager.update_list(self.user.id, saved_list.id, {"name": another_name})

        self.assertEqual(newly_updated_list.name, another_name)
        self.assertEqual(newly_updated_list.owner, self.user.id)

    def test_list_task_interaction(self):
        list = self.manager.create_list(List("list", self.user.id))

        first_sub_task = self.manager.create_task(Task("first sub task", self.user.id))
        second_sub_task = self.manager.create_task(Task("second sub task", self.user.id))
        third_sub_task = self.manager.create_task(Task("third sub task", self.user.id))

        _ = self.manager.update_list(self.user.id, list.id,
                                     {"tasks": [first_sub_task.id, second_sub_task.id, third_sub_task.id]})

        sub_tasks_id = [task.id for task in self.manager.get_list_sub_tasks(self.user.id, list.id)]

        self.assertTrue(first_sub_task.id in sub_tasks_id)
        self.assertTrue(second_sub_task.id in sub_tasks_id)
        self.assertTrue(third_sub_task.id in sub_tasks_id)

        _ = self.manager.update_list(self.user.id, list.id,
                                     {"tasks": [first_sub_task.id]})

        new_sub_tasks_id = [task.id for task in self.manager.get_list_sub_tasks(self.user.id, list.id)]

        self.assertFalse(first_sub_task.id in new_sub_tasks_id)
        self.assertTrue(second_sub_task.id in new_sub_tasks_id)
        self.assertTrue(third_sub_task.id in new_sub_tasks_id)

    def test_sub_list_interaction(self):
        list = self.manager.create_list(List("list", self.user.id))

        first_sub_list = self.manager.create_list(List("first sub list", self.user.id))
        second_sub_list = self.manager.create_list(List("second sub list", self.user.id))
        third_sub_list = self.manager.create_list(List("third sub list", self.user.id))

        _ = self.manager.update_list(self.user.id, list.id,
                                     {"lists": [first_sub_list.id, second_sub_list.id, third_sub_list.id]})

        sub_lists_id = [list.id for list in self.manager.get_sub_lists(self.user.id, list.id)]

        self.assertTrue(first_sub_list.id in sub_lists_id)
        self.assertTrue(second_sub_list.id in sub_lists_id)
        self.assertTrue(third_sub_list.id in sub_lists_id)

        _ = self.manager.update_list(self.user.id, list.id,
                                     {"lists": [first_sub_list.id, second_sub_list.id]})

        new_sub_lists_id = [list.id for list in self.manager.get_sub_lists(self.user.id, list.id)]

        self.assertFalse(first_sub_list.id in new_sub_lists_id)
        self.assertFalse(second_sub_list.id in new_sub_lists_id)
        self.assertTrue(third_sub_list.id in new_sub_lists_id)

    def test_create_plan(self):

        date = "2018-07-07"
        start_time = "12:00"
        finish_time = "14:00"
        start_date = "2018-07-07"
        finish_date = "2018-07-12"
        repetition_start_date = "2018-07-12"
        repetition_finish_date = "2018-08-07"
        days_of_week = [2, 4]
        number = 3
        days = 3
        weeks = 2
        months = 0
        years = 0

        time = Time(start_time, finish_time, date)
        kind = Kind(number,
                    days_of_week=days_of_week,
                    year=years,
                    month=months,
                    week=weeks,
                    day=days)

        plan = Plan(time, start_date, finish_date, repetition_start_date, repetition_finish_date, kind)
        saved_plan = self.manager.create_plan(plan)

        self.assertEqual(saved_plan.id, 1)
        self.assertEqual(saved_plan.time.start_time, start_time)
        self.assertEqual(saved_plan.time.finish_time, finish_time)
        self.assertEqual(saved_plan.time.date, date)

        self.assertEqual(saved_plan.start_date, start_date)
        self.assertEqual(saved_plan.finish_date, finish_date)
        self.assertEqual(saved_plan.repetition_start_date, repetition_start_date)
        self.assertEqual(saved_plan.repetition_finish_date, repetition_finish_date)

        self.assertEqual(saved_plan.repetition.number_of_periods, number)
        self.assertEqual(saved_plan.repetition.days_of_week, days_of_week)
        self.assertEqual(saved_plan.repetition.year, years)
        self.assertEqual(saved_plan.repetition.month, months)
        self.assertEqual(saved_plan.repetition.week, weeks)
        self.assertEqual(saved_plan.repetition.day, days)

        time2 = Time(finish_time, start_time, date)
        kind2 = Kind(number)

        plan2 = Plan(time2, start_date, finish_date, repetition_start_date, repetition_finish_date, kind2)

        with self.assertRaises(InvalidDate):
            _ = self.manager.create_plan(plan2)

        time3 = Time(start_time, finish_time, date)
        kind3 = Kind(number)

        plan3 = Plan(time2, finish_date, start_date, repetition_start_date, repetition_finish_date, kind2)

        with self.assertRaises(InvalidDate):
            _ = self.manager.create_plan(plan3)

        time3 = Time(start_time, finish_time, date)
        kind3 = Kind(number)

        plan3 = Plan(time2, start_date, finish_date, repetition_finish_date, repetition_start_date, kind2)

        with self.assertRaises(InvalidDate):
            _ = self.manager.create_plan(plan3)

        with self.assertRaises(TypeError):
            _ = self.manager.create_plan("Not a plan")

    def test_delete_plan(self):
        plan = Plan(repetition_start_date="2018-07-12")
        saved_plan = self.manager.create_plan(plan)
        plan_id = saved_plan.id

        with self.assertRaises(ObjectNotFound):
            self.manager.delete_plan(1234, plan_id)

        with self.assertRaises(ObjectNotFound):
            self.manager.delete_plan(self.user.id, 1234)

        self.manager.delete_plan(self.user.id, plan_id)
        with self.assertRaises(ObjectNotFound):
            self.manager.get_plan(self.user.id, plan_id)

    def test_update_plan(self):
        date = "2018-07-07"
        start_time = "12:00"
        finish_time = "14:00"
        start_date = "2018-07-07"
        finish_date = "2018-07-12"
        repetition_start_date = "2018-07-12"
        repetition_finish_date = "2018-08-07"
        days_of_week = [2, 4]
        number = 3
        days = 3
        weeks = 2
        months = 0
        years = 0

        time = Time(start_time, finish_time, date)
        kind = Kind(number)

        plan = Plan(time,
                    finish_date=finish_date,
                    repetition_start_date=repetition_start_date,
                    repetition=kind)
        saved_plan = self.manager.create_plan(plan)

        update_params = {
            "start_time": start_time,
            "finish_time": None,
            "date": date,
            "start_date": start_date,
            "finish_date": None,
            "repetition_finish_date": repetition_finish_date,
            "number_of_periods": number,
            "days_of_week": days_of_week,
            "days": days,
            "weeks": weeks,
            "months": months,
            "years": years
        }

        updated_plan = self.manager.update_plan(self.user.id, saved_plan.id, update_params)

        self.assertEqual(updated_plan.id, saved_plan.id)
        self.assertEqual(updated_plan.time.start_time, start_time)
        self.assertIsNone(updated_plan.time.finish_time)
        self.assertEqual(updated_plan.time.date, date)

        self.assertEqual(updated_plan.start_date, start_date)
        self.assertIsNone(updated_plan.finish_date)
        self.assertEqual(updated_plan.repetition_start_date, repetition_start_date)
        self.assertEqual(updated_plan.repetition_finish_date, repetition_finish_date)

        self.assertEqual(updated_plan.repetition.number_of_periods, number)
        self.assertEqual(updated_plan.repetition.days_of_week, days_of_week)
        self.assertEqual(updated_plan.repetition.year, years)
        self.assertEqual(updated_plan.repetition.month, months)
        self.assertEqual(updated_plan.repetition.week, weeks)
        self.assertEqual(updated_plan.repetition.day, days)

        invalid_params = {
            "start_time": finish_time,
            "finish_time": start_time
        }

        with self.assertRaises(InvalidDate):
            _ = self.manager.update_plan(self.user.id, saved_plan.id, invalid_params)

        another_invalid_params = {
            "start_date": finish_date,
            "finish_date": start_date
        }

        with self.assertRaises(InvalidDate):
            _ = self.manager.update_plan(self.user.id, saved_plan.id, another_invalid_params)

        new_invalid_params = {
            "repetition_start_date": repetition_finish_date,
            "repetition_finish_date": repetition_start_date
        }

        with self.assertRaises(InvalidDate):
             _ = self.manager.update_plan(self.user.id, saved_plan.id, new_invalid_params)

    def test_create_priority(self):
        priority = Priority("priority", True)
        saved_priority = self.manager.create_priority(priority)

        self.assertEqual(saved_priority.id, 1)
        self.assertEqual(saved_priority.value, "priority")
        self.assertTrue(saved_priority.default)

        self.assertEqual(self.manager.storage.get_default_priority().id, saved_priority.id)

        priority2 = Priority("another priority")
        saved_priority2 = self.manager.create_priority(priority2)

        self.assertEqual(saved_priority2.id, 2)
        self.assertEqual(saved_priority2.value, "another priority")
        self.assertFalse(saved_priority2.default)

        self.assertEqual(self.manager.storage.get_default_priority().id, saved_priority.id)

        with self.assertRaises(TypeError):
            _ = self.manager.create_priority("Not a priority")

    def test_delete_priority(self):
        priority = self.manager.create_priority(Priority("priority"))
        priority_id = priority.id

        with self.assertRaises(ObjectNotFound):
            self.manager.delete_priority(1234, priority_id)

        with self.assertRaises(ObjectNotFound):
            self.manager.delete_priority(self.user.id, 1234)

        self.manager.delete_priority(self.user.id, priority_id)
        with self.assertRaises(ObjectNotFound):
            self.manager.get_priority(self.user.id, priority_id)

    def test_update_priority(self):
        priority = Priority("priority", True)
        saved_priority = self.manager.create_priority(priority)

        self.assertEqual(self.manager.storage.get_default_priority().id, saved_priority.id)

        updated_priority = self.manager.update_priority(self.user.id, saved_priority.id, {"value": "new_priority"})

        self.assertEqual(updated_priority.value, "new_priority")

        new_priority = Priority("future_default_priority")
        new_saved_priority = self.manager.create_priority(new_priority)

        new_updated_priority = self.manager.update_priority(self.user.id, new_saved_priority.id, {"default": True})

        self.assertEqual(self.manager.storage.get_default_priority().id, new_updated_priority.id)

        new_default_priority = Priority("new_default_priority", True)
        new_default_saved_priority = self.manager.create_priority(new_default_priority)

        self.assertEqual(self.manager.storage.get_default_priority().id, new_default_saved_priority.id)

    def test_create_status(self):
        status = Status("status", True)
        saved_status = self.manager.create_status(status)

        self.assertEqual(saved_status.id, 1)
        self.assertEqual(saved_status.value, "status")
        self.assertTrue(saved_status.default)

        self.assertEqual(self.manager.storage.get_default_status().id, saved_status.id)

        status2 = Status("another status")
        saved_status2 = self.manager.create_status(status2)

        self.assertEqual(saved_status2.id, 2)
        self.assertEqual(saved_status2.value, "another status")
        self.assertFalse(saved_status2.default)

        self.assertEqual(self.manager.storage.get_default_status().id, saved_status.id)

        with self.assertRaises(TypeError):
            _ = self.manager.create_status("Not a status")

    def test_delete_status(self):
        status = self.manager.create_status(Status("status"))
        status_id = status.id

        with self.assertRaises(ObjectNotFound):
            self.manager.delete_status(1234, status_id)

        with self.assertRaises(ObjectNotFound):
            self.manager.delete_status(self.user.id, 1234)

        self.manager.delete_status(self.user.id, status_id)
        with self.assertRaises(ObjectNotFound):
            self.manager.get_status(self.user.id, status_id)

    def test_update_status(self):
        status = Status("status", True)
        saved_status = self.manager.create_status(status)

        self.assertEqual(self.manager.storage.get_default_status().id, saved_status.id)

        updated_status = self.manager.update_status(self.user.id, saved_status.id, {"value": "new_status"})

        self.assertEqual(updated_status.value, "new_status")

        new_status = Status("future_default_status")
        new_saved_status = self.manager.create_status(new_status)

        new_updated_status = self.manager.update_status(self.user.id, new_saved_status.id, {"default": True})

        self.assertEqual(self.manager.storage.get_default_status().id, new_updated_status.id)

        new_default_status = Status("new_default_status", True)
        new_default_saved_status = self.manager.create_status(new_default_status)

        self.assertEqual(self.manager.storage.get_default_status().id, new_default_saved_status.id)

    def test_create_type(self):
        type = Type("type", True)
        saved_type = self.manager.create_type(type)

        self.assertEqual(saved_type.id, 1)
        self.assertEqual(saved_type.value, "type")
        self.assertTrue(saved_type.default)

        self.assertEqual(self.manager.storage.get_default_type().id, saved_type.id)

        type2 = Type("another type")
        saved_type2 = self.manager.create_type(type2)

        self.assertEqual(saved_type2.id, 2)
        self.assertEqual(saved_type2.value, "another type")
        self.assertFalse(saved_type2.default)

        self.assertEqual(self.manager.storage.get_default_type().id, saved_type.id)

        with self.assertRaises(TypeError):
            _ = self.manager.create_type("Not a type")

    def test_delete_type(self):
        type = self.manager.create_type(Type("type"))
        type_id = type.id

        with self.assertRaises(ObjectNotFound):
            self.manager.delete_type(1234, type_id)

        with self.assertRaises(ObjectNotFound):
            self.manager.delete_type(self.user.id, 1234)

        self.manager.delete_type(self.user.id, type_id)
        with self.assertRaises(ObjectNotFound):
            self.manager.get_type(self.user.id, type_id)

    def test_update_type(self):
        type = Type("type", True)
        saved_type = self.manager.create_type(type)

        self.assertEqual(self.manager.storage.get_default_type().id, saved_type.id)

        updated_type = self.manager.update_type(self.user.id, saved_type.id, {"value": "new_type"})

        self.assertEqual(updated_type.value, "new_type")

        new_type = Type("future_default_type")
        new_saved_type = self.manager.create_type(new_type)

        new_updated_type = self.manager.update_type(self.user.id, new_saved_type.id, {"default": True})

        self.assertEqual(self.manager.storage.get_default_type().id, new_updated_type.id)

        new_default_type = Type("new_default_type", True)
        new_default_saved_type = self.manager.create_type(new_default_type)

        self.assertEqual(self.manager.storage.get_default_type().id, new_default_saved_type.id)

    def test_create_tag(self):
        tag = Tag("tag", True)
        saved_tag = self.manager.create_tag(tag)

        self.assertEqual(saved_tag.id, 1)
        self.assertEqual(saved_tag.value, "tag")
        self.assertTrue(saved_tag.default)

        tag2 = Tag("another tag")
        saved_tag2 = self.manager.create_tag(tag2)

        self.assertEqual(saved_tag2.id, 2)
        self.assertEqual(saved_tag2.value, "another tag")
        self.assertFalse(saved_tag2.default)

        default_tags_id = [tag.id for tag in self.manager.storage.get_default_tags()]

        self.assertTrue(saved_tag.id in default_tags_id)
        self.assertFalse(saved_tag2.id in default_tags_id)

        with self.assertRaises(TypeError):
            _ = self.manager.create_tag("Not a tag")

    def test_delete_tag(self):
        tag = self.manager.create_tag(Tag("tag"))
        tag_id = tag.id

        with self.assertRaises(ObjectNotFound):
            self.manager.delete_tag(1234, tag_id)

        with self.assertRaises(ObjectNotFound):
            self.manager.delete_tag(self.user.id, 1234)

        self.manager.delete_tag(self.user.id, tag_id)
        with self.assertRaises(ObjectNotFound):
            self.manager.get_tag(self.user.id, tag_id)

    def test_update_Tag(self):
        tag = Tag("tag", True)
        saved_tag = self.manager.create_tag(tag)

        updated_tag = self.manager.update_tag(self.user.id, saved_tag.id, {"value": "new_tag"})

        self.assertEqual(updated_tag.value, "new_tag")

        new_tag = Tag("future_default_tag")
        new_saved_tag = self.manager.create_tag(new_tag)

        new_updated_tag = self.manager.update_tag(self.user.id, new_saved_tag.id, {"default": True})

        new_default_tag = Tag("new_default_tag", True)
        new_default_saved_tag = self.manager.create_tag(new_default_tag)

        default_tags_id = [tag.id for tag in self.manager.storage.get_default_tags()]

        self.assertTrue(saved_tag.id in default_tags_id)
        self.assertTrue(new_updated_tag.id in default_tags_id)
        self.assertTrue(new_default_saved_tag.id in default_tags_id)


if __name__ == '__main__':
    unittest.main()
