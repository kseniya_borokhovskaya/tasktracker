from setuptools import setup, find_packages

setup(
    name='tracker_lib',
    author='Kseniya Borokhovskaya',
    version='1.5',
    packages=find_packages()
)
