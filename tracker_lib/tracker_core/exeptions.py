"""
    This module includes library exceptions

    Classes:
        ObjectNotFound
        UserAlreadyExists
        PermissionsError
        InvalidDate
        BlockedTask
        NotValidConfig
        StorageNotFound

"""


class ObjectNotFound(Exception):
    def __init__(self, type, value):
        self.type = type
        self.value = value

    def __str__(self):
        return "Can't find object {0} with id {1}.".format(self.type, self.value)


class UserAlreadyExists(Exception):
    def __init__(self, login):
        self.login = login

    def __str__(self):
        return "User with '{}' login already exists.".format(self.login)


class PermissionsError(Exception):
    def __init__(self, user_id):
        self.user_id = user_id

    def __str__(self):
        return "User {} has no permissins for this action".format(self.user_id)


class InvalidDate(Exception):
    def __init__(self, start, finish):
        self.start = start
        self.finish = finish

    def __str__(self):
        return "The start {0} have to be earlier than finish {1}".format(self.start, self.finish)


class BlockedTask(Exception):
    def __init__(self, task):
        self.task = task

    def __str__(self):
        return "You're trying to change blocked task {0.id}: {0.name}".format(self.task)


class StorageNotFound(Exception):
    def __str__(self):
        return "Can't find storage."
