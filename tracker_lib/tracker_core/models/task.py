"""
    This module includes data models for
    representing task, task's connections and information.

    Classes:
        Task
        SubTasksRelations
        BlockingTasksRelations
        Priority
        Status
        Type
        Tag
"""

from datetime import datetime


class Task:
    """
        Task class

        Attributes:
            id -- task identifier
            name -- task name
            owner -- owner of task
            assignee -- person, who need to do the task
            status -- task status (Status object)
            priority -- task priority (Priority object)
            type -- task type (Type object)

            description -- task description
            tags -- tags with which the task is marked
            place -- place where task is happening

            creation_date -- date when the task was created
            date -- date when the task're set on
            start_time -- time when the task are starting
            finish_time -- time when the task are ended
            duration -- duration of the task

            plan -- id of Plan object, that set rules for repeating task
            reminder -- if of reminder object
    """
    def __init__(self, name, owner, assignee=None,
                 type=None, priority=None, status=None,
                 start_time=None, finish_time=None,
                 date=None, duration=0,
                 plan=None, reminder=None,
                 description="", tags=None, place=""):
        self.id = None
        self.name = name
        self.owner = owner
        self.assignee = assignee
        self.type = type
        self.priority = priority
        self.status = status

        self.creation_date = datetime.now()
        self.date = date
        self.start_time = start_time
        self.finish_time = finish_time
        self.duration = duration

        self.plan = plan
        self.reminder = reminder

        self.isDone = False
        self.done_date = None

        self.description = description
        self.tags = tags
        self.place = place


class SubTasksRelations:
    """
        Sub Tasks Relations class

        Represents connections between tasks and it's sub tasks

        Attributes:
            id -- relation identifier
            parent_id -- id of parent task
            child_id -- id of child task (sub task of parent task)
    """
    def __init__(self, task_id, subtask_id):
        self.id = None
        self.parent_id = task_id
        self.child_id = subtask_id


class BlockingTasksRelations:
    """
        Blocking Tasks Relations class

        Represents connections between blocked and blocking tasks

        Attributes:
            id -- relation identifier
            blocking_id -- id of task which are blocking another task
            blocked_id -- id of blocked task (it can't be changed)
    """
    def __init__(self, blocked_id, blocking_id):
        self.id = None
        self.blocking_id = blocking_id
        self.blocked_id = blocked_id


class Priority:
    """
        Priority class

        Represents priority of task

        Attributes:
            id -- unique identifier
            value -- string with title of priority
            default -- shows if priority is default
    """
    def __init__(self, value, default=False):
        self.id = None
        self.value = value
        self.default = default


class Status:
    """
        Status class

        Represents status of task

        Attributes:
            id -- unique identifier
            value -- string with title of status
            default -- shows if status is default
    """
    def __init__(self, value, default=False):
        self.id = None
        self.value = value
        self.default = default


class Type:
    """
        Type class

        Represents type of task

        Attributes:
            id -- unique identifier
            value -- string with title of type
            default -- shows if type is default
    """
    def __init__(self, value, default=False):
        self.id = None
        self.value = value
        self.default = default


class Tag:
    """
        Tag class

        Represents tag of task

        Args:
            id -- unique identifier
            value -- string with title of tag
            default -- shows if tag is default
    """
    def __init__(self, value, default=False):
        self.id = None
        self.value = value
        self.default = default

