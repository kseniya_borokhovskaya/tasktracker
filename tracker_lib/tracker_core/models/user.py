"""
    This module includes data models for
    representing user and team.

    Classes:
        User
        Team
"""


class User:
    """
        User class

        Attributes:
            id -- user identifier
            name -- user name
            login -- user unique login
            email -- user email
    """
    def __init__(self, name, login, email):
        self.id = None
        self.name = name
        self.login = login
        self.email = email


class Team:
    """
        User class

        Attributes:
            id -- team identifier
            name -- team name
            creator -- creator of the team
    """
    def __init__(self, name, creator, users):
        self.id = None
        self.name = name
        self.creator = creator


