"""
    This module includes data models for
    representing objects related with time.

    Classes:
        Time
        Plan
        Reminder
        Kind
"""


class Time:
    """
        Time class

        Represents information about date and time.

        Attributes:
            id -- time identifier
            start_time -- start time
            finish_time -- finish time
            date -- date
    """
    def __init__(self,
                 start_time=None,
                 finish_time=None,
                 date=None):

        self.id = None
        self.start_time = start_time
        self.finish_time = finish_time
        self.date = date


class Plan:
    """
        Plan class

        Represents plans of task.

        Attributes:
            id -- plan identifier
            time -- list of Time objects
            start_date
            finish_date
            is_repeat -- true if repetition is set
            repetition_start_date
            repetition_finish_date
            repetition -- list of Kind objects
    """
    def __init__(self, time=None, start_date=None, finish_date=None,
                 repetition_start_date=None, repetition_finish_date=None,
                 repetition=None):
        self.id = None
        self.time = time
        self.start_date = start_date
        self.finish_date = finish_date
        self.is_repeat = True if repetition is not None else False
        self.repetition_start_date = repetition_start_date
        self.repetition_finish_date = repetition_finish_date
        self.repetition = repetition


class Reminder:
    """
        Reminder class

        Attributes:
            id -- reminder identifier
            dates -- dates
            is_on_begin_time
            is_on_finish_time
            reminders -- list of Kind objects
    """
    def __init__(self, dates=None, is_on_begin_time=False,
                 is_on_finish_time=False, reminders=None):
        self.id = None
        self.dates = dates
        self.is_on_begin_time = is_on_begin_time
        self.is_on_finish_time = is_on_finish_time
        self.reminders = reminders


class Kind:
    """
        Kind class

        Attributes:
            id -- plan identifier
            number_of_periods -- represents number of repetitions
            days_of_week -- on witch days of week repetition is set
            year -- number of years
            month -- number of months
            week -- number of weeks
            day -- number of days
    """
    def __init__(self, number_of_periods=0,
                 days_of_week=None,
                 year=0, month=0,
                 week=0, day=0):
        self.id = None
        self.number_of_periods = number_of_periods
        self.days_of_week = days_of_week
        self.year = year
        self.month = month
        self.week = week
        self.day = day
