"""
    This module includes data models for
    representing list and list's connections.

    Classes:
        List
        TaskListRelations
        SubListRelations
"""


class List:
    """
        List class

        Attributes:
            id -- list identifier
            name -- list name
            owner -- owner of list
            isListOfTasks -- None if list is empty, True if it contains tasks, False if it contains lists
    """
    def __init__(self, name, owner):
        self.id = None
        self.name = name
        self.owner = owner
        self.isListOfTasks = None


class TaskListRelations:
    """
        Task List Relations class

        Represents connections between tasks and lists

        Attributes:
            id -- relation identifier
            list_id -- id of list
            task_id -- id of task that contained in list
    """
    def __init__(self, list_id, task_id):
        self.id = None
        self.list_id = list_id
        self.task_id = task_id


class SubListRelations:
    """
        Sub List Relations class

        Represents connections between list and it's sub lists

        Attributes:
            id -- relation identifier
            parent_id -- id of parent list
            child_id -- id of child list (sub list of parent list)
        """
    def __init__(self, list_id, sublist_id):
        self.id = None
        self.parent_id = list_id
        self.child_id = sublist_id

