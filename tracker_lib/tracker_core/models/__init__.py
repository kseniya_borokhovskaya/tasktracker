"""
    This package includes models classes for representing lib's entities.

    Modules:
        task - task class and classes, connected with tasks
        list - list class and classes, connected with lists
        time - store information about everything, connected with time (includes plan, reminder)
        user - user class

"""