"""
    TaskTracker library. Includes packages and modules to managing daily tasks interaction.

    Packages:
        models - classes, witch represents library entities
        manager - includes module for managing data
        data_connectors - provides methods for worcking with database

    Modules:
        exceptions - includes library exceptions
        logging - includes log's functions and decorators

"""