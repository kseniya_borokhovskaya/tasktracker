"""This module includes log functions and decorators for writing error and debug logs"""


import logging
from functools import wraps

from tracker_core.exeptions import (
    ObjectNotFound,
    PermissionsError,
    InvalidDate,
    BlockedTask
)


def get_logger():
    """Returns library logger."""
    logger = logging.getLogger("tracker_core")
    return logger


def write_error_log(func):
    """ Decorates function for logging exception, raises them again"""
    @wraps(func)
    def wrapper(*args, **kwargs):
        logger = get_logger()

        try:
            return func(*args, **kwargs)

        except ObjectNotFound as e:
            logger.error("Error {0}: {1}".format(e.__class__.__name__, str(e)))
            raise

        except PermissionsError as e:
            logger.error("Error {0}: {1}".format(e.__class__.__name__, str(e)))
            raise

        except InvalidDate as e:
            logger.error("Error {0}: {1}".format(e.__class__.__name__, str(e)))
            raise

        except BlockedTask as e:
            logger.error("Error {0}: {1}".format(e.__class__.__name__, str(e)))
            raise

        except Exception as e:
            logger.error(e)
            raise

    return wrapper


def write_debug_log(func):
    """ Decorates function for logging functions's entrance and exit"""
    @wraps(func)
    def wrapper(*args, **kwargs):
        logger = get_logger()

        logger.debug("Called {} function".format(func.__name__))

        try:
            return func(*args, **kwargs)

        except Exception as e:
            logger.debug("Exited {0} function with {0} error".format(func.__name__, e.__class__.__name__))
            raise

        finally:
            logger.debug("{} function finished execution".format(func.__name__))

    return wrapper










