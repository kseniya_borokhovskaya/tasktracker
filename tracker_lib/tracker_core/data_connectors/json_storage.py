"""
    This module provides class with functions
    for accessing data in json format.

    Class:
        JsonStorage - contains main functions to work with data

    Constant:
        DEFAULT_CORE_DIR - directory, data will be save to if there are no path passed.

"""
import os.path

import jsonpickle

from tracker_core.models.list import (
    SubListRelations,
    TaskListRelations
)
from tracker_core.models.task import (
    BlockingTasksRelations,
    SubTasksRelations
)
from tracker_core.exeptions import UserAlreadyExists


class JsonStorage:
    """
           This class contains functions which allow
           add, update, delete, retrieve and perform
           simple operations on data models stored in
           database.

    """

    def __init__(self, users_path, path=None):

        if not os.path.exists(os.path.expanduser(users_path)):
            os.mkdir(os.path.expanduser(users_path))

        self.user_file = os.path.join(users_path, "users.json")
        self.user_id_file = os.path.join(users_path, "user_id.json")

        self.path = path

        if path is not None and os.path.exists(os.path.expanduser(path)):
            os.mkdir(os.path.expanduser(path))

        self.task_file = os.path.join(self.path, "tasks.json") if path is not None else None
        self.sub_task_file = os.path.join(self.path, "sub_tasks.json") if path is not None else None
        self.blocking_task_file = os.path.join(self.path, "blocking_tasks.json") if path is not None else None

        self.list_file = os.path.join(self.path, "lists.json") if path is not None else None
        self.list_task_file = os.path.join(self.path, "list_task.json") if path is not None else None
        self.sub_list_file = os.path.join(self.path, "sub_lists.json") if path is not None else None

        self.tag_file = os.path.join(self.path, "tags.json") if path is not None else None
        self.type_file = os.path.join(self.path, "types.json") if path is not None else None
        self.priority_file = os.path.join(self.path, "priorities.json") if path is not None else None
        self.status_file = os.path.join(self.path, "statuses.json") if path is not None else None
        self.time_file = os.path.join(self.path, "time.json") if path is not None else None
        self.plan_file = os.path.join(self.path, "plans.json") if path is not None else None
        self.reminder_file = os.path.join(self.path, "reminders.json") if path is not None else None
        self.kind_file = os.path.join(self.path, "kind.json") if path is not None else None

        self.id_file = os.path.join(self.path, "id.json") if path is not None else None

    def reload_data_path(self, path):
        self.path = path

        if not os.path.exists(os.path.expanduser(path)):
            os.mkdir(os.path.expanduser(path))

        self.task_file = os.path.join(self.path, "tasks.json")
        self.sub_task_file = os.path.join(self.path, "sub_tasks.json")
        self.blocking_task_file = os.path.join(self.path, "blocking_tasks.json")

        self.list_file = os.path.join(self.path, "lists.json")
        self.list_task_file = os.path.join(self.path, "list_task.json")
        self.sub_list_file = os.path.join(self.path, "sub_lists.json")

        self.tag_file = os.path.join(self.path, "tags.json")
        self.type_file = os.path.join(self.path, "types.json")
        self.priority_file = os.path.join(self.path, "priorities.json")
        self.status_file = os.path.join(self.path, "statuses.json")
        self.time_file = os.path.join(self.path, "time.json")
        self.plan_file = os.path.join(self.path, "plans.json")
        self.reminder_file = os.path.join(self.path, "reminders.json")
        self.kind_file = os.path.join(self.path, "kind.json")

        self.id_file = os.path.join(self.path, "id.json")

    @staticmethod
    def _get_all_entities(file):
        if os.path.isfile(file):
            with open(os.path.expanduser(file), 'r') as f:
                return jsonpickle.decode(f.read())
        else:
            return []

    @staticmethod
    def _save_entities(file, entities):
        if file is not None:
            with open(os.path.expanduser(file), 'w') as f:
                return f.write(jsonpickle.encode(entities))

    def _add_entity(self, file, entity):
        entities = self._get_all_entities(file)
        if len(entities) == 0:
            entity.id = 1
        else:
            entity.id = entities[len(entities) - 1].id + 1
        entities.append(entity)
        self._save_entities(file, entities)
        return entity

    def _delete_entity(self, file, id):
        entities = self._get_all_entities(file)
        for entity in entities:
            if str(entity.id) == str(id):
                entities.remove(entity)
                return self._save_entities(file, entities)

    def _change_entity(self, file, new_entity):
        entities = self._get_all_entities(file)
        for index in range(len(entities)):
            if entities[index].id == new_entity.id:
                entities[index] = new_entity
                self._save_entities(file, entities)
                return new_entity

    def _get_entity(self, file, id):
        entities = self._get_all_entities(file)
        for entity in entities:
            if str(entity.id) == str(id):
                return entity

    # users
    def get_all_users(self):
        return self._get_all_entities(self.user_file)

    def save_users(self, users):
        return self._save_entities(self.user_file, users)

    def add_user(self, new_user):
        users = self.get_all_users()
        for user in users:
            if user.login == new_user.login:
                raise UserAlreadyExists(user.login)
        return self._add_entity(self.user_file, new_user)

    def delete_user(self, user_id):
        return self._delete_entity(self.user_file, user_id)

    def change_user(self, new_user):
        return self._change_entity(self.user_file, new_user)

    def get_user(self, user_id):
        return self._get_entity(self.user_file, user_id)

    def get_user_by_login(self, login):
        users = self.get_all_users()
        for user in users:
            if user.login == login:
                return user

    # tasks
    def get_all_tasks(self, user_id):
        tasks = self._get_all_entities(self.task_file)
        user_tasks = [task for task in tasks if task.owner == user_id or task.assignee == user_id]
        return user_tasks

    def save_tasks(self, tasks):
        return self._save_entities(self.task_file, tasks)

    def add_task(self, task):
        return self._add_entity(self.task_file, task)

    def delete_task(self, task_id):
        self._delete_entity(self.task_file, task_id)
        self.delete_blocking_connections(task_id)
        self.delete_sub_tasks_connections(task_id)
        self.delete_task_list_connections(task_id)

    def change_task(self, new_task):
        return self._change_entity(self.task_file, new_task)

    def get_task(self, task_id):
        return self._get_entity(self.task_file, task_id)

    def get_filtered_tasks(self, user_id, filter):
        all_tasks = self.get_all_tasks(user_id)
        tasks = []
        for task in all_tasks:
            if filter(task) is True:
                tasks.append(task)
        return tasks

    # sub tasks and blocking tasks
    def save_sub_task_relation(self, relations):
        return self._save_entities(self.sub_task_file, relations)

    def add_sub_task_relation(self, relation):
        return self._add_entity(self.sub_task_file, relation)

    def get_all_subtasks_relations(self):
        return self._get_all_entities(self.sub_task_file)

    def delete_sub_task_relation(self, relation_id):
        self._delete_entity(self.sub_task_file, relation_id)

    def delete_sub_tasks_connections(self, task_id):
        relations = self.get_all_subtasks_relations()
        for relation in relations:
            if relation.parent_id == task_id or relation.child_id == task_id:
                self.delete_sub_task_relation(relation.id)

    def save_blocking_relation(self, relations):
        return self._save_entities(self.blocking_task_file, relations)

    def add_blocking_relation(self, relation):
        return self._add_entity(self.blocking_task_file, relation)

    def get_all_blocking_relations(self):
        return self._get_all_entities(self.blocking_task_file)

    def delete_blocking_relation(self, relation_id):
        self._delete_entity(self.blocking_task_file, relation_id)

    def delete_blocking_connections(self, task_id):
        relations = self.get_all_blocking_relations()
        for relation in relations:
            if relation.blocking_id == task_id or relation.blocked_id == task_id:
                self.delete_blocking_relation(relation.id)

    def add_sub_task_to_task(self, task_id, sub_task_id):
        relations = SubTasksRelations(task_id, sub_task_id)
        return self.add_sub_task_relation(relations)

    def remove_sub_task_from_task(self, task_id, sub_task_id):
        relations = self.get_all_subtasks_relations()
        for relation in relations:
            if (relation.parent_id == task_id
                    and relation.child_id == sub_task_id):
                relations.remove(relation)
        self.save_sub_task_relation(relations)

    def change_task_sub_tasks_relations(self, task_id, sub_tasks_id):
        relations = self.get_all_subtasks_relations()
        for sub_task_id in sub_tasks_id:
            add = True
            for relation in relations:
                if relation.parent_id == task_id and relation.child_id == sub_task_id:
                    relations.remove(relation)
                    add = False
                    continue
            if add:
                relations.append(self.add_sub_task_to_task(task_id, sub_task_id))
        self.save_sub_task_relation(relations)

    def get_task_sub_tasks(self, task_id):
        sub_tasks = []
        relations = self.get_all_subtasks_relations()
        for relation in relations:
            if relation.parent_id == task_id:
                sub_tasks.append(self.get_task(relation.child_id))
        return sub_tasks

    def add_blocking_task_to_task(self, task_id, blocking_task_id):
        relations = BlockingTasksRelations(task_id, blocking_task_id)
        return self.add_blocking_relation(relations)

    def remove_blocking_task_from_task(self, task_id, blocking_task_id):
        relations = self.get_all_blocking_relations()
        for relation in relations:
            if (relation.blocking_id == blocking_task_id
                    and relation.blocked_task_id == task_id):
                relations.remove(relation)
        self.save_blocking_relation(relations)

    def change_blocking_task_relations(self, task_id, blocking_tasks_id):
        relations = self.get_all_blocking_relations()
        for blocking_task_id in blocking_tasks_id:
            add = True
            for relation in relations:
                if relation.blocked_id == task_id and relation.blocking_id == blocking_task_id:
                    relations.remove(relation)
                    add = False
                    continue
            if add:
                relations.append( self.add_blocking_task_to_task(task_id, blocking_task_id))
            self.save_blocking_relation(relations)

    def get_blocking_tasks(self, task_id):
        blocking_tasks = []
        relations = self.get_all_blocking_relations()
        for relation in relations:
            if relation.blocked_id == task_id:
                blocking_tasks.append(self.get_task(relation.blocked_id))
        return blocking_tasks

    def get_blocked_tasks(self, task_id):
        blocked_tasks = []
        relations = self.get_all_subtasks_relations()
        for relation in relations:
            if relation.blocking_id == task_id:
                blocked_tasks.append(self.get_task(relation.blocking_id))
        return blocked_tasks

    # lists
    def get_all_lists(self, user_id):
        lists = self._get_all_entities(self.list_file)
        user_lists = [list for list in lists if list.owner == user_id]
        return user_lists

    def save_lists(self, lists):
        return self._save_entities(self.list_file, lists)

    def add_list(self, task_list):
        return self._add_entity(self.list_file, task_list)

    def delete_list(self, list_id):
        self._delete_entity(self.list_file, list_id)
        self.delete_list_task_connections(list_id)
        self.delete_sub_list_connections(list_id)

    def change_list(self, new_list):
        return self._change_entity(self.list_file, new_list)

    def get_list(self, list_id):
        return self._get_entity(self.list_file, list_id)

    # list-task relations and sub lists

    def save_list_task_relation(self, relations):
        return self._save_entities(self.list_task_file, relations)

    def add_list_task_relation(self, relation):
        return self._add_entity(self.list_task_file, relation)

    def get_all_list_task_relations(self):
        return self._get_all_entities(self.list_task_file)

    def delete_list_task_relation(self, relation_id):
        self._delete_entity(self.list_task_file, relation_id)

    def delete_list_task_connections(self, list_id):
        relations = self.get_all_list_task_relations()
        for relation in relations:
            if relation.list_id == list_id:
                self.delete_list_task_relation(relation.id)

    def delete_task_list_connections(self, task_id):
        relations = self.get_all_list_task_relations()
        for relation in relations:
            if relation.task_id == task_id:
                self.delete_list_task_relation(relation.id)

    def save_sub_lists_relations(self, relations):
        return self._save_entities(self.sub_list_file, relations)

    def add_sub_list_relation(self, relation):
        return self._add_entity(self.sub_list_file, relation)

    def get_all_sub_list_relations(self):
        return self._get_all_entities(self.sub_list_file)

    def delete_sub_list_relation(self, relation_id):
        self._delete_entity(self.sub_list_file, relation_id)

    def delete_sub_list_connections(self, list_id):
        relations=self.get_all_sub_list_relations()
        for relation in relations:
            if relation.parent_id == list_id or relation.child_id == list_id:
                self.delete_sub_list_relation(relation.id)

    def add_task_to_list(self, list_id, task_id):
        relations = TaskListRelations(list_id, task_id)
        return self.add_list_task_relation(relations)

    def remove_task_from_list(self, list_id, task_id):
        relations = self.get_all_list_task_relations()
        for relation in relations:
            if (relation.list_id == list_id
                    and relation.task_id == task_id):
                relations.remove(relation)
        self.save_list_task_relation(relations)

    def change_list_task_relations(self, list_id, tasks_id):
        relations = self.get_all_list_task_relations()
        for task_id in tasks_id:
            add = True
            for relation in relations:
                if relation.list_id == list_id and relation.task_id == task_id:
                    relations.remove(relation)
                    add = False
                    continue
            if add:
                relations.append(self.add_task_to_list(list_id, task_id))
        self.save_list_task_relation(relations)

    def get_list_tasks(self, list_id):
        tasks = []
        relations = self.get_all_list_task_relations()
        for relation in relations:
            if relation.list_id == list_id:
                tasks.append(self.get_task(relation.task_id))
        return tasks

    def add_list_to_list(self, list_id, sub_list_id):
        relations = SubListRelations(list_id, sub_list_id)
        return self.add_sub_list_relation(relations)

    def remove_list_from_list(self, list_id, sub_list_id):
        relations = self.get_all_sub_list_relations()
        for relation in relations:
            if (relation.parent_id == list_id
                    and relation.child_id == sub_list_id):
                relations.remove(relation)
        self.save_sub_lists_relations(relations)

    def change_sub_lists_relations(self, list_id, sub_lists_id):
        relations = self.get_all_sub_list_relations()
        for sub_list_id in sub_lists_id:
            add = True
            for relation in relations:
                if relation.parent_id == list_id and relation.child_id == sub_list_id:
                    relations.remove(relation)
                    add = False
                    continue
            if add:
                relations.append(self.add_list_to_list(list_id, sub_list_id))
        self.save_sub_lists_relations(relations)

    def get_sub_lists(self, list_id):
        lists = []
        relations = self.get_all_sub_list_relations()
        for relation in relations:
            if relation.parent_id == list_id:
                lists.append(self.get_list(relation.child_id))
        return lists

    # plan
    def get_all_plans(self):
        return self._get_all_entities(self.plan_file)

    def save_plans(self, plans):
        return self._save_entities(self.plan_file, plans)

    def add_plan(self, plan):
        return self._add_entity(self.plan_file, plan)

    def delete_plan(self, plan_id):
        return self._delete_entity(self.plan_file, plan_id)

    def change_plan(self, new_plan):
        return self._change_entity(self.plan_file, new_plan)

    def get_plan(self, plan_id):
        return self._get_entity(self.plan_file, plan_id)

    def set_task_plan(self, task_id, plan_id):
        task = self.get_task(task_id)
        task.plan = plan_id
        self.change_task(task)

    def remove_task_plan(self, task_id):
        task = self.get_task(task_id)
        task.plan = None
        self.change_task(task)

    # reminder

    def get_all_reminders(self):
        return self._get_all_entities(self.reminder_file)

    def save_reminders(self, reminders):
        return self._save_entities(self.reminder_file, reminders)

    def add_reminder(self, reminder):
        return self._add_entity(self.reminder_file, reminder)

    def delete_reminder(self, reminder_id):
        return self._delete_entity(self.reminder_file, reminder_id)

    def change_reminder(self, new_reminder):
        return self._change_entity(self.reminder_file, new_reminder)

    def get_reminder(self, reminder_id):
        return self._get_entity(self.reminder_file, reminder_id)

    def set_reminder_of_task(self, task_id, reminder_id):
        task = self.get_task(task_id)
        task.priority = reminder_id
        self.change_task(task)

    def remove_reminder_of_task(self, task_id):
        task = self.get_task(task_id)
        task.priority = None
        self.change_task(task)

    # priority
    def get_all_priorities(self):
        return self._get_all_entities(self.priority_file)

    def save_priorities(self, priorities):
        return self._save_entities(self.priority_file, priorities)

    def add_priority(self, priority):
        return self._add_entity(self.priority_file, priority)

    def delete_priority(self, priority_id):
        return self._delete_entity(self.priority_file, priority_id)

    def change_priority(self, new_priority):
        return self._change_entity(self.priority_file, new_priority)

    def get_priority(self, priority_id):
        return self._get_entity(self.priority_file, priority_id)

    def set_priority_of_task(self, task_id, priority_id):
        task = self.get_task(task_id)
        task.priority = priority_id
        self.change_task(task)

    def remove_priority_of_task(self, task_id):
        task = self.get_task(task_id)
        task.priority = None
        self.change_task(task)

    def get_default_priority(self):
        priorities = self.get_all_priorities()
        for priority in priorities:
            if priority.default:
                return priority
        return None

    def change_default_priority(self, new_default_priority):
        priorities = self.get_all_priorities()
        for priority in priorities:
            if priority.default:
                priority.default = False
            if priority.id == new_default_priority.id:
                priority.default = True
        self.save_priorities(priorities)

    # status
    def get_all_statuses(self):
        return self._get_all_entities(self.status_file)

    def save_statuses(self, statuses):
        return self._save_entities(self.status_file, statuses)

    def add_status(self, status):
        return self._add_entity(self.status_file, status)

    def delete_status(self, status_id):
        return self._delete_entity(self.status_file, status_id)

    def change_status(self, new_status):
        return self._change_entity(self.status_file, new_status)

    def get_status(self, status_id):
        return self._get_entity(self.status_file, status_id)

    def set_status_of_task(self, task_id, status_id):
        task = self.get_task(task_id)
        task.type = status_id
        self.change_task(task)

    def remove_status_of_task(self, task_id):
        task = self.get_task(task_id)
        task.status = None
        self.change_task(task)

    def get_default_status(self):
        statuses = self.get_all_statuses()
        for status in statuses:
            if status.default:
                return status
        return None

    def change_default_status(self, new_default_status):
        statuses = self.get_all_statuses()
        for status in statuses:
            if status.default:
                status.default = False
            if status.id == new_default_status.id:
                status.default = True
        self.save_statuses(statuses)

    # type
    def get_all_types(self):
        return self._get_all_entities(self.type_file)

    def save_types(self, types):
        return self._save_entities(self.type_file, types)

    def add_type(self, type):
        return self._add_entity(self.type_file, type)

    def delete_type(self, type_id):
        return self._delete_entity(self.type_file, type_id)

    def change_type(self, new_type):
        return self._change_entity(self.type_file, new_type)

    def get_type(self, type_id):
        return self._get_entity(self.type_file, type_id)

    def set_type_of_task(self, task_id, type_id):
        task = self.get_task(task_id)
        task.type = type_id
        self.change_task(task)

    def remove_type_of_task(self, task_id):
        task = self.get_task(task_id)
        task.type = None
        self.change_task(task)

    def get_default_type(self):
        types = self.get_all_types()
        for type in types:
            if type.default:
                return type
        return None

    def change_default_type(self, new_default_type):
        types = self.get_all_types()
        for type in types:
            if type.default:
                type.default = False
            if type.id == new_default_type.id:
                type.default = True
        self.save_types(types)

    # tag
    def get_all_tags(self):
        return self._get_all_entities(self.tag_file)

    def save_tags(self, tags):
        return self._save_entities(self.tag_file, tags)

    def add_tag(self, tag):
        return self._add_entity(self.tag_file, tag)

    def delete_tag(self, tag_id):
        return self._delete_entity(self.tag_file, tag_id)

    def change_tag(self, new_tag):
        return self._change_entity(self.tag_file, new_tag)

    def get_tag(self, tag_id):
        return self._get_entity(self.tag_file, tag_id)

    def add_tag_to_task(self, task_id, tag_id):
        task = self.get_task(task_id)
        task.tags.append(tag_id)
        self.change_task(task)

    def remove_tag_from_task(self, task_id, tag_id):
        task = self.get_task(task_id)
        task.tags.remove(tag_id)
        self.change_task(task)

    def get_default_tags(self):
        tags = self.get_all_tags()
        default_tags = []
        for tag in tags:
            if tag.default:
                default_tags.append(tag)
        return default_tags

    def change_default_tag(self, tag):
        tag.default = True
        self.change_tag(tag)




