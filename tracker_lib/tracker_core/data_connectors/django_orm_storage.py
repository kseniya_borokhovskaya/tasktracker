from tracker_core.models.list import (
    SubListRelations,
    TaskListRelations,
    Task,
    Type,
    Priority,
    Status,
    Tag
)
from tracker_core.models.task import (
    List
    BlockingTasksRelations,
    SubTasksRelations
)
from datetime import datetime


class DjangoORMStorage:
    def __init__(self,
                 tasks_model,
                 lists_model,
                 plans_model,
                 types_model,
                 priorities_model,
                 statuses_model,
                 tags_model,
                 user_model):
        self.Task = tasks_model
        self.List = lists_model
        self.Plan = plans_model
        self.Type = types_model
        self.Priority = priorities_model
        self.Status = statuses_model
        self.Tag = tags_model
        self.User = user_model

    def convert_task_to_orm_task(self, task):
        return self.Task(name=task.name,
                         owner=task.owner,
                         assignee=task.assignee,
                         type=task.type,
                         priority=task.priority,
                         status=task.status,
                         date=task.date,
                         start_time=task.start_time,
                         finish_time=task.finish_time,
                         description=task.description,
                         tags=task.tags,
                         plan=task.plan,
                         reminder=task.reminder,
                         place=task.place)

    def convert_orm_task_to_lib_task(self, task):
        return Task(name=task.name,
                    owner=task.owner,
                    assignee=task.assignee,
                    type=task.type,
                    priority=task.priority,
                    status=task.status,
                    date=task.date,
                    start_time=task.start_time,
                    finish_time=task.finish_time,
                    description=task.description,
                    tags=task.tags,
                    plan=task.plan,
                    reminder=task.reminder,
                    place=task.place)

    def add_task(self, task):
        return self.Task.objects.create(self.convert_task_to_orm_task(task))

    def get_all_tasks(self, user_id):
        return self.Task.objects.all()

    def get_task(self, task_id):
        return self.Task.objects.get(id=task_id)

    def delete_task(self, task_id):
        self.get_task(task_id).delete()

    def change_task(self, task):
        saved_task = self.Task.objects.get(id=task.id)
            self.convert_task_to_orm_task(task)
        saved_task.id = task.id
        saved_task.save()

    def get_filtered_tasks(self, owner=None, assignee=None, name=None, priority=None,
                            type=None, status=None, date=None, tag=None):
        tasks = self.Task.objects.all()
        if status is not None:
            tasks=tasks.filter(priority=self.Status.objects.get(id=status))
        if name is not None:
            tasks = tasks.filter(name=name)
        if priority is not None:
            tasks = tasks.filter(priority=self.Priority.objects.get(id=priority))
        if owner is not None:
            tasks=tasks.filter(owner=self.User.objects.get(id=owner))
        if assignee is not None:
            tasks=tasks.filter(owner=self.User.objects.get(id=assignee))
        if type != -1:
            tasks=tasks.filter(priority=self.Status.objects.get(id=type))
        if date is not None:
            tasks=tasks.filter(date=date)
        if tag is not None:
            tasks=tasks.filter(priority=self.Type.objects.get(id=tag))

        return tasks

