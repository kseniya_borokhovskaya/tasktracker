"""
    This module provides class with functions
    for accessing data layer.

    Class:
        DataManager - interlayer between database and client side

    Decorators:
        check_user_existence - check if user in data base
        check_task - check if task in data base and
                    if user have rights to interact with task
        check_list - check if list in data base and
                    if user have rights to interact with list

"""

from tracker_core.exeptions import (
    ObjectNotFound,
    PermissionsError,
    InvalidDate,
    BlockedTask
)
from tracker_core.logging import get_logger
from tracker_core.models.list import List
from tracker_core.models.task import (
    Task,
    Priority,
    Type,
    Status,
    Tag
)
from tracker_core.models.user import User
from tracker_core.models.schedule import (
    Plan,
    Reminder
)
from tracker_core.logging import (
    write_error_log,
    write_debug_log
)
from datetime import datetime


def check_user_existence(func):
    """ Decorates function, checking the existence of user """
    def wrapper(manager, user_id, *args, **kwargs):

        user = manager.storage.get_user(user_id)

        if user is None:
            raise ObjectNotFound(User, user_id)

        return func(manager, user_id, *args, **kwargs)

    return wrapper


def check_task(func):
    """
        Decorates function, checking the existence of task
        and user permission on the task.
    """
    def wrapper(manager, user_id, task_id, *args, **kwargs):
        task = manager.storage.get_task(task_id)

        if task is None:
            raise ObjectNotFound(Task, task_id)

        if task.owner != user_id and task.assignee != user_id:
            raise PermissionsError(user_id)

        return func(manager, user_id, task_id, *args, **kwargs)

    return wrapper


def check_list(func):
    """
        Decorates function, checking the existence of list
        and user permission on the list.
    """
    def wrapper(manager, user_id, list_id, *args, **kwargs):
        list = manager.storage.get_list(list_id)

        if list is None:
            raise ObjectNotFound(List, list_id)

        if list.owner != user_id:
            raise PermissionsError(user_id)

        return func(manager, user_id, list_id, *args, **kwargs)

    return wrapper


def check_obj(get_function):

    def decorator(func):

        def wrapper(manager, user_id, obj_id, *args, **kwargs):
            obj = getattr(manager.storage, get_function)(obj_id)

            if obj is None:
                raise ObjectNotFound(type(obj), obj_id)

            return func(manager, user_id, obj_id, *args, **kwargs)

        return wrapper

    return decorator


class DataManager:
    """
        This class includes CRUD
        functions to work with data

        Attributes:
            storage: object implementing data-storage interface

        Functions:

        """
    def __init__(self, storage):
        self.storage = storage

    # work with tasks
    @write_debug_log
    @write_error_log
    def create_task(self, task):
        """
        Save task in data base.

        Args:
            task -- task (Taks object) to create in db

        """
        if not isinstance(task, Task):
            raise TypeError("Can't save task to data base, because object is not Task type")

        self._validate_task_time(task)

        if task.type is None:
            task.type = self.storage.get_default_type()
        if task.priority is None:
            task.priority = self.storage.get_default_priority()
        if task.status is None:
            task.status = self.storage.get_default_status()
        if task.tags is None:
            task.tags = self.storage.get_default_tags()
        if task.assignee is None:
            task.assignee = task.owner

        saved_task = self.storage.add_task(task)

        get_logger().info("Task {0.id}: {0.name} was saved to data base".format(task))

        return saved_task

    @write_debug_log
    @write_error_log
    @check_user_existence
    def get_all_tasks(self, user_id):
        """
        Get tasks from data base.

        Args:
            user_id -- id of user performing the operation

        Returns:
            All tasks saved in data base

        """
        return self.storage.get_all_tasks(user_id)

    @write_error_log
    @write_debug_log
    @check_user_existence
    @check_task
    def get_task(self, user_id, task_id):
        """
            Get task from data base.

            Args:
                user_id -- id of user performing the operation
                task_id -- id of sought task

            Returns:
                Task with defined id

        """
        return self.storage.get_task(task_id)

    @write_error_log
    @write_debug_log
    @check_user_existence
    @check_task
    def get_filtered_tasks(self, user_id, filter):
        """
            Get tasks from data base by some filter.

            Args:
                user_id -- id of user performing the operation
                filter -- lambda that verifies the equality
                          task's properties to some value

            Returns:
                Task with defined filter

        """
        return self.storage.get_filtered_tasks(user_id, filter)

    @write_error_log
    @write_debug_log
    @check_user_existence
    @check_task
    def delete_task(self, user_id, task_id):
        """
            Delete task from data base.

            Args:
                user_id -- id of user performing the operation
                task_id -- id of task aim to be deleted

        """

        self._check_if_blocked_task(task_id)

        self.storage.delete_task(task_id)

        get_logger().info("Task {} was deleted from data base".format(task_id))

    @write_error_log
    @write_debug_log
    @check_user_existence
    @check_task
    def update_task(self, user_id, task_id, params):
        """
            Update task in data base.

            Args:
                user_id -- id of user performing the operation
                task_id -- id of task aim to be updated
                params -- dict of properties of task for updating:
                          "property" (key): new_value (value)
        """
        task = self.storage.get_task(task_id)

        self._check_if_blocked_task(task_id)

        actions_dict = self._get_task_actions_mapping()

        for param in params:
            if param in actions_dict:
                actions_dict[param](task, params[param])

        self._validate_task_time(task)
        updated_task = self.storage.change_task(task)

        get_logger().info("Task {0.id}: {0.name} was updated in data base".format(task))

        return updated_task

    @write_error_log
    @write_debug_log
    def _check_if_blocked_task(self, task_id):
        task = self.storage.get_task(task_id)

        if len(self.storage.get_blocking_tasks(task_id)) > 0:
            raise BlockedTask(task)

    @write_error_log
    @write_debug_log
    def _validate_task_time(self, task):
        if task.start_time is not None and task.finish_time is not None:
            start_time = datetime.strptime(task.start_time, '%H:%M').time()
            finish_time = datetime.strptime(task.finish_time, '%H:%M').time()

            if start_time > finish_time:
                raise InvalidDate(start_time, finish_time)

            get_logger().info(
                'The time of the {0.id}: "{0.name}" task was changed'.format(task))

    @write_debug_log
    def _get_task_actions_mapping(self):
        return {
            "name": self.change_task_name,
            "date": self.change_task_date,
            "start_time": self.change_start_time,
            "finish_time": self.change_finish_time,
            "duration": self.change_duration,
            "type": self.change_task_type,
            "priority": self.change_task_priority,
            "status": self.change_task_status,
            "plan": self.change_task_plan,
            "reminder": self.change_task_reminder,
            "description": self.change_description,
            "ancestor": self.change_task_ancestor,
            "tags": self.change_task_tags_relations,
            "sub_tasks": self.change_task_sub_tasks_relations,
            "blocking": self.change_task_blocking_tasks_relations
        }

    @write_debug_log
    def change_task_name(self, task, name):
        task.name = name

        get_logger().info('The name of the {0.id} task was changed to {0.name}'.format(task))

    @write_debug_log
    def change_task_date(self, task, date):
        task.date = date

        get_logger().info('The date of the {0.id}: "{0.name}" task was changed to {1}'.format(task, date))

    @write_debug_log
    def change_start_time(self, task, start_time):
        task.start_time = start_time

    @write_debug_log
    def change_finish_time(self, task, finish_time):
        task.finish_time = finish_time

    @write_debug_log
    def change_duration(self, task, duration):
        task.duration = duration

        get_logger().info('The duration of the {0.id}: "{0.name}" task was changed to {1}'.format(task, duration))

    @write_debug_log
    def change_task_type(self, task, type):
        task.type = type

        get_logger().info('The type of the {0.id}: "{0.name}" task was changed to {1}'.format(task, type.value))

    @write_debug_log
    def change_task_priority(self, task, priority):
        task.priority = priority

        get_logger().info('The duration of the {0.id}: "{0.name}" task was changed to {1}'.format(task, priority.value))

    @write_debug_log
    def change_task_status(self, task, status):
        task.status = status

        get_logger().info('The duration of the {0.id}: "{0.name}" task was changed to {1}'.format(task, status.value))

    @write_debug_log
    def change_task_plan(self, task, plan):
        task.plan = plan

        get_logger().info('The duration of the {0.id}: "{0.name}" task was changed to {1}'.format(task, plan.id))

    @write_debug_log
    def change_task_reminder(self, task, reminder):
        task.reminder = reminder

        get_logger().info('The duration of the {0.id}: "{0.name}" task was changed to {1}'.format(task, reminder.id))

    @write_debug_log
    def change_description(self, task, description):
        task.description = description

        get_logger().info('The duration of the {0.id}: "{0.name}" task was changed to {1}'.format(task, description))

    @write_debug_log
    def change_task_tags_relations(self, task, tags):
        for tag in tags:
            if tag in task.tags:
                task.tags.remove(tag)
                get_logger().info(
                    'The "{0.name}"(id: {0.id}) task was unmarked by {1} tag'.format(task, tag.value))
            else:
                task.tags.append(tag)
                get_logger().info(
                    'The "{0.name}"(id: {0.id}) task was marked by {1} tag'.format(task, tag.value))

    @write_debug_log
    def change_task_ancestor(self, task, ancestor):
        self.storage.change_task_sub_tasks_relations(ancestor, task.id)
        if ancestor is None:
            get_logger().info(
                'The ancestor of the {0.id}: "{0.name}" task was deleted'.format(task))
        else:
            get_logger().info(
                'The ancestor of the {0.id}: "{0.name}" task was changed to {1.id}: {1.name}'.format(task, ancestor))

    @write_debug_log
    def change_task_sub_tasks_relations(self, task, sub_tasks):
        self.storage.change_task_sub_tasks_relations(task.id, sub_tasks)

        names_str = " ".join(str(sub_tasks))
        get_logger().info(
            'The relations of the {0.id}: "{0.name}" task with {1} sub tasks was changed'.format(task, names_str))

    @write_debug_log
    def change_task_blocking_tasks_relations(self, task, blocking_tasks):
        self.storage.change_blocking_task_relations(task.id, blocking_tasks)

        names_str = " ".join(str(blocking_tasks))
        get_logger().info(
            'The relations of the {0.id}: "{0.name}" task with {1} blocking tasks was changed'.format(task, names_str))

    @write_error_log
    @write_debug_log
    @check_user_existence
    @check_task
    def get_task_sub_tasks(self, user_id, task_id):
        """
            Get task from data base.

            Args:
                user_id -- id of user performing the operation
                task_id -- id of task

            Returns:
                Sub tasks with defined id

        """
        return self.storage.get_task_sub_tasks(task_id)

    @write_error_log
    @write_debug_log
    @check_user_existence
    @check_task
    def get_blocking_tasks(self, user_id, task_id):
        """
            Get task from data base.

            Args:
                user_id -- id of user performing the operation
                task_id -- id of task

            Returns:
                Blocking tasks with defined id

        """
        return self.storage.get_blocking_tasks(task_id)

    @write_error_log
    @write_debug_log
    @check_user_existence
    @check_task
    def get_blocked_tasks(self, user_id, task_id):
        """
            Get task from data base.

            Args:
                user_id -- id of user performing the operation
                task_id -- id of task

            Returns:
                Blocked task with defined id

        """
        return self.storage.get_blocked_tasks(task_id)

    # work with lists
    @write_debug_log
    @write_error_log
    def create_list(self, list):
        """
        Save list in data base.

        Args:
            list -- list (List object) to create in db

        """
        if not isinstance(list, List):
            raise TypeError("Can't save list to data base, because object is not List type")

        saved_list = self.storage.add_list(list)

        get_logger().info("List {0.id}: {0.name} was saved to data base".format(list))

        return saved_list

    @check_user_existence
    @write_debug_log
    def get_all_lists(self, user_id):
        """
        Get lists from data base.

        Args:
            user_id -- id of user performing the operation

        Returns:
            All lists saved in data base

        """
        return self.storage.get_all_lists(user_id)

    @write_debug_log
    @write_error_log
    @check_user_existence
    @check_list
    def get_list(self, user_id, list_id):
        """
            Get list from data base.

            Args:
                user_id -- id of user performing the operation
                list_id -- id of sought list

            Returns:
                List with defined id

        """
        return self.storage.get_list(list_id)

    @write_error_log
    @write_debug_log
    @check_user_existence
    @check_list
    def get_list_sub_tasks(self, user_id, list_id):
        """
            Get list from data base.

            Args:
                user_id -- id of user performing the operation
                list_id -- id of list

            Returns:
                Sub tasks with defined id

        """
        return self.storage.get_list_tasks(list_id)

    @write_error_log
    @write_debug_log
    @check_user_existence
    @check_list
    def get_sub_lists(self, user_id, list_id):
        """
            Get list from data base.

            Args:
                user_id -- id of user performing the operation
                list_id -- id of list

            Returns:
                Sub lists tasks with defined id

        """
        return self.storage.get_sub_lists(list_id)

    @write_debug_log
    @write_error_log
    @check_user_existence
    @check_list
    def delete_list(self, user_id, list_id):
        """
            Delete lists from data base.

            Args:
                user_id -- id of user performing the operation
                list_id -- id of list aim to be deleted

        """
        self.storage.delete_list(list_id)

        get_logger().info("List {} was deleted from data base".format(list_id))

    @write_debug_log
    @write_error_log
    @check_user_existence
    @check_list
    def update_list(self, user_id, list_id, params):
        """
            Update lists in data base.

            Args:
                user_id -- id of user performing the operation
                list_id -- id of list aim to be updated
                params -- dict of properties of list for updating:
                          "property" (key): new_value (value)
        """
        list = self.storage.get_list(list_id)

        dict = self._get_list_actions_mapping()

        for param in params:
            if param in dict:
                dict[param](list, params[param])

        return self.storage.change_list(list)

    @write_debug_log
    def _get_list_actions_mapping(self):
        return {
            "name": self.change_list_name,
            "ancestor": self.change_list_ancestor,
            "lists": self.change_sub_lists_relations,
            "tasks": self.change_list_task_relations
        }

    @write_debug_log
    def change_list_name(self, list, name):
        list.name = name

        get_logger().info(
            'The name of the {0.id}: "{0.name}" list was changed to {1}'.format(list, name))

    @write_debug_log
    def change_list_ancestor(self, list, ancestor):
        self.storage.change_sub_lists_relations(ancestor, list.id)

        if ancestor is None:
            get_logger().info(
                'The ancestor of the {0.id}: "{0.name}" list was deleted'.format(list))
        else:
            get_logger().info(
                'The ancestor of the {0.id}: "{0.name}" list was changed to {1.id}: {1.name}'.format(list, ancestor))

    @write_debug_log
    def change_sub_lists_relations(self, list, sub_lists):
        self.storage.change_sub_lists_relations(list.id, sub_lists)

        names_str = " ".join(str(sub_lists))
        get_logger().info(
            'The relations of the {0.id}: "{0.name}" list with {1} sub lists was changed'.format(list, names_str))

    @write_debug_log
    def change_list_task_relations(self, list, tasks):
        self.storage.change_list_task_relations(list.id, tasks)

        names_str = " ".join(str(tasks))
        get_logger().info(
            'The relations of the {0.id}: "{0.name}" list with {1} sub lists was changed'.format(list, names_str))

    # plans
    @write_debug_log
    @write_error_log
    def create_plan(self, plan):
        """
        Save plan in data base.

        Args:
            plan -- plan (Plan object) to create in db

        """
        if not isinstance(plan, Plan):
            raise TypeError("Can't save plan to data base, because object is not Plan type")

        self._validate_plan_dates(plan)
        saved_plan = self.storage.add_plan(plan)

        get_logger().info("Plan {0.id} was saved to data base".format(plan))

        return saved_plan

    @write_debug_log
    @write_error_log
    @check_user_existence
    def get_all_plans(self, user_id):
        """
        Get plans from data base.

        Args:
            user_id -- id of user performing the operation

        Returns:
            All plans saved in data base

        """
        return self.storage.get_all_plans()

    @write_debug_log
    @write_error_log
    @check_user_existence
    @check_obj("get_plan")
    def get_plan(self, user_id, plan_id):
        """
            Get plan from data base.

            Args:
                user_id -- id of user performing the operation
                plan_id -- id of sought plan

            Returns:
                Plan with defined id

        """
        plan = self.storage.get_plan(plan_id)
        if plan is not None:
            return plan
        else:
            raise ObjectNotFound(Plan, plan_id)

    @write_debug_log
    @write_error_log
    @check_user_existence
    @check_obj("get_plan")
    def delete_plan(self, user_id, plan_id):
        """
            Delete plan from data base.

            Args:
                user_id -- id of user performing the operation
                plan_id -- id of plan aim to be deleted

        """
        self.storage.delete_plan(plan_id)

        get_logger().info("Plan {} was deleted from data base".format(plan_id))

    @write_debug_log
    @write_error_log
    @check_user_existence
    def update_plan(self, user_id, plan_id, params):
        """
            Update plan in data base.

            Args:
                user_id -- id of user performing the operation
                plan_id -- id of plan aim to be updated
                params -- dict of properties of plan for updating:
                          "property" (key): new_value (value)
        """
        plan = self.storage.get_plan(plan_id)

        dict = self._get_plan_actions_mapping()

        for param in params:
            if param in dict:
                dict[param](plan, params[param])

        self._validate_plan_dates(plan)
        updated_plan = self.storage.change_plan(plan)

        get_logger().info("Plan {} was updated from data base".format(plan_id))

        return updated_plan

    @write_debug_log
    @write_error_log
    def _validate_plan_dates(self, plan):
        if plan.start_date is not None and plan.finish_date is not None:
            start_date = datetime.strptime(plan.start_date, "%Y-%m-%d").date()
            finish_date = datetime.strptime(plan.finish_date, "%Y-%m-%d").date()

            if finish_date < start_date:
                raise InvalidDate(start_date, finish_date)

        if plan.repetition_start_date is not None and plan.repetition_finish_date is not None:
            start_date = datetime.strptime(plan.repetition_start_date, "%Y-%m-%d").date()
            finish_date = datetime.strptime(plan.repetition_finish_date, "%Y-%m-%d").date()

            if finish_date < start_date:
                raise InvalidDate(start_date, finish_date)

        if plan.time is not None and plan.time.start_time is not None and plan.time.finish_time is not None:
            start_time = datetime.strptime(plan.time.start_time, '%H:%M').time()
            finish_time = datetime.strptime(plan.time.finish_time, '%H:%M').time()

            if start_time > finish_time:
                raise InvalidDate(start_time, finish_time)

    @write_debug_log
    def _get_plan_actions_mapping(self):
        return {
            "date": self.change_plan_date,
            "start_time": self.change_plan_start_time,
            "finish_time": self.change_plan_finish_time,
            "start_date": self.change_plan_start_date,
            "finish_date": self.change_plan_finish_date,
            "repetition_start_date": self.change_repetition_start_date,
            "repetition_finish_date": self.change_repetition_finish_date,
            "number": self.change_number_of_repeats,
            "days_of_week": self.change_days_of_week,
            "days": self.change_days,
            "weeks": self.change_weeks,
            "months": self.change_months,
            "years": self.change_years
        }

    def change_plan_date(self, plan, date):
        plan.time.date = date

    def change_plan_start_time(self, plan, start_time):
        plan.time.start_time = start_time

    def change_plan_finish_time(self, plan, finish_time):
        plan.time.finish_time = finish_time

    def change_plan_start_date(self, plan, start_date):
        plan.start_date = start_date

    def change_plan_finish_date(self, plan, finish_date):
        plan.finish_date = finish_date

    def change_repetition_start_date(self, plan, repetition_start_date):
        plan.repetition_start_date = repetition_start_date

    def change_repetition_finish_date(self, plan, repetition_finish_date):
        plan.repetition_finish_date = repetition_finish_date

    def change_number_of_repeats(self, plan, number_of_periods):
        plan.repetition.number_of_periods = number_of_periods

    def change_days_of_week(self, plan, days_of_week):
        plan.repetition.days_of_week = days_of_week

    def change_days(self, plan, day):
        plan.repetition.day = day

    def change_weeks(self, plan, week):
        plan.repetition.week = week

    def change_months(self, plan, month):
        plan.repetition.month = month

    def change_years(self, plan, year):
        plan.repetition.year = year

    def create_reminder(self, reminder):
        self.storage.add_reminder(reminder)

        get_logger().info("Reminder {0.id} was saved to data base".format(reminder))

    def get_all_reminders(self, user_id):
        return self.storage.get_all_reminders()

    def get_reminder(self, user_id, reminder_id):
        reminder = self.storage.get_reminder(reminder_id)
        if reminder is not None:
            return reminder
        else:
            raise ObjectNotFound(Reminder, reminder_id)

    def delete_reminder(self, user_id, reminder_id):
        self.storage.delete_reminder(reminder_id)

        get_logger().info("Reminder {} was deleted from data base".format(reminder_id))

    def update_reminder(self, user_id, reminder_id, params):
        reminder = self.storage.get_reminder(reminder_id)

        dict = self.get_reminder_actions_mapping()

        for param in params:
            if param in dict:
                dict[param](reminder, params[param])

        self.storage.change_reminder(reminder)

    def get_reminder_actions_mapping(self):
        return {
            # "start_dates": self.change_plan_start_dates,
            # "finish_dates": self.change_plan_finish_dates,
            # "repetition_finish_date": self.change_repetition_finish_date,
            # "number": self.cnahge_number,
            # "repetition_period_type": self.change_repetition_period_type
        }

    @write_debug_log
    @write_error_log
    def create_priority(self, priority):
        """
        Save priority in data base.

        Args:
            priority -- priority to create in db

        """
        if not isinstance(priority, Priority):
            raise TypeError("Can't save priority to data base, because object is not Priority type")

        saved_priority = self.storage.add_priority(priority)

        if priority.default:
            self.storage.change_default_priority(saved_priority)

        get_logger().info("Priority {0.id}: {0.value} was saved to data base".format(priority))

        return saved_priority

    @write_debug_log
    @write_error_log
    @check_user_existence
    def get_all_priorities(self, user_id):
        """
        Get priorities from data base.

        Args:
            user_id -- id of user performing the operation

        Returns:
            All priorities saved in data base

        """
        return self.storage.get_all_priorities()

    @write_debug_log
    @write_error_log
    @check_user_existence
    @check_obj("get_priority")
    def get_priority(self, user_id, priority_id):
        """
            Get priority from data base.

            Args:
                user_id -- id of user performing the operation
                priority_id -- id of sought priority

            Returns:
                Priority with defined id

        """
        priority = self.storage.get_priority(priority_id)
        if priority is not None:
            return priority
        else:
            raise ObjectNotFound(Priority, priority_id)

    @write_debug_log
    @write_error_log
    @check_user_existence
    @check_obj("get_priority")
    def delete_priority(self, user_id, priority_id):
        """
            Delete priority from data base.

            Args:
                user_id -- id of user performing the operation
                priority_id -- id of priority aim to be deleted

        """
        self.storage.delete_priority(priority_id)

        get_logger().info("Priority {} was deleted from data base".format(priority_id))

    @write_debug_log
    @write_error_log
    @check_user_existence
    def update_priority(self, user_id, priority_id, params):
        """
            Update priority in data base.

            Args:
                user_id -- id of user performing the operation
                priority_id -- id of priority aim to be updated
                params -- dict of properties of priority for updating:
                          "property" (key): new_value (value)
        """
        priority = self.storage.get_priority(priority_id)

        if 'value' in params:
            priority.value = params['value']

        saved_priority = self.storage.change_priority(priority)

        if 'default' in params:
            self.storage.change_default_priority(priority)

        return saved_priority

    @write_debug_log
    @write_error_log
    def create_status(self, status):
        """
        Save status in data base.

        Args:
            status -- status to create in db

        """
        if not isinstance(status, Status):
            raise TypeError("Can't save status to data base, because object is not Status type")

        saved_status = self.storage.add_status(status)

        if status.default:
            self.storage.change_default_status(status)

        get_logger().info("Status {0.id}: {0.value} was saved to data base".format(status))

        return saved_status

    @write_debug_log
    @write_error_log
    @check_user_existence
    def get_all_statuses(self, user_id):
        """
        Get statuses from data base.

        Args:
            user_id -- id of user performing the operation

        Returns:
            All statuses saved in data base

        """
        return self.storage.get_all_statuses()

    @write_debug_log
    @write_error_log
    @check_user_existence
    @check_obj("get_status")
    def get_status(self, user_id, status_id):
        """
            Get status from data base.

            Args:
                user_id -- id of user performing the operation
                status_id -- id of sought status

            Returns:
                Status with defined id

        """
        return self.storage.get_status(status_id)

    @write_debug_log
    @write_error_log
    @check_user_existence
    @check_obj("get_status")
    def delete_status(self, user_id, status_id):
        """
            Delete status from data base.

            Args:
                user_id -- id of user performing the operation
                status_id -- id of status aim to be deleted

        """
        self.storage.delete_status(status_id)

        get_logger().info("Status {} was deleted from data base".format(status_id))

    @write_debug_log
    @write_error_log
    @check_user_existence
    def update_status(self, user_id, status_id, params):
        """
            Update status in data base.

            Args:
                user_id -- id of user performing the operation
                status_id -- id of status aim to be updated
                params -- dict of properties of status for updating:
                          "property" (key): new_value (value)
        """
        status = self.storage.get_status(status_id)

        if 'value' in params:
            status.value = params['value']

        saved_status = self.storage.change_status(status)

        if 'default' in params:
            self.storage.change_default_status(status)

        return saved_status

    @write_debug_log
    @write_error_log
    def create_type(self, type):
        """
        Save type in data base.

        Args:
            type -- type to create in db

        """
        if not isinstance(type, Type):
            raise TypeError("Can't save task type to data base, because object is not task Type type")

        saved_type = self.storage.add_type(type)

        if type.default:
            self.storage.change_default_type(type)

        get_logger().info("Type {0.id}: {0.value} was saved to data base".format(type))

        return saved_type

    @write_debug_log
    @write_error_log
    @check_user_existence
    def get_all_types(self, user_id):
        """
        Get types from data base.

        Args:
            user_id -- id of user performing the operation

        Returns:
            All types saved in data base

        """
        return self.storage.get_all_types()

    @write_debug_log
    @write_error_log
    @check_user_existence
    @check_obj("get_type")
    def get_type(self, user_id, type_id):
        """
            Get type from data base.

            Args:
                user_id -- id of user performing the operation
                type_id -- id of sought type

            Returns:
                Type with defined id

        """
        return self.storage.get_type(type_id)

    @write_debug_log
    @write_error_log
    @check_user_existence
    @check_obj("get_type")
    def delete_type(self, user_id, type_id):
        """
            Delete type from data base.

            Args:
                user_id -- id of user performing the operation
                type_id -- id of type aim to be deleted

        """
        self.storage.delete_type(type_id)

        get_logger().info("Type {} was deleted from data base".format(type_id))

    @write_debug_log
    @write_error_log
    @check_user_existence
    def update_type(self, user_id, type_id, params):
        """
            Update types in data base.

            Args:
                user_id -- id of user performing the operation
                type_id -- id of type aim to be updated
                params -- dict of properties of type for updating:
                          "property" (key): new_value (value)
        """
        type = self.storage.get_type(type_id)

        if 'value' in params:
            type.value = params['value']

        saved_type = self.storage.change_type(type)

        if 'default' in params:
            self.storage.change_default_type(type)

        return saved_type

    @write_debug_log
    @write_error_log
    def create_tag(self, tag):
        """
        Save tag in data base.

        Args:
            tag -- tag to create in db

        """
        if not isinstance(tag, Tag):
            raise TypeError("Can't save tag to data base, because object is not Tag type")

        saved_tag = self.storage.add_tag(tag)

        get_logger().info("Priority {0.id}: {0.value} was saved to data base".format(tag))

        return saved_tag

    @write_debug_log
    @write_error_log
    @check_user_existence
    def get_all_tags(self, user_id):
        """
        Get tags from data base.

        Args:
            user_id -- id of user performing the operation

        Returns:
            All tags saved in data base

        """
        return self.storage.get_all_tags()

    @write_debug_log
    @write_error_log
    @check_user_existence
    @check_obj("get_tag")
    def get_tag(self, user_id, tag_id):
        """
            Get tag from data base.

            Args:
                user_id -- id of user performing the operation
                tag_id -- id of sought tag

            Returns:
                Tag with defined id

        """
        return self.storage.get_tag(tag_id)

    @write_debug_log
    @write_error_log
    @check_user_existence
    @check_obj("get_tag")
    def delete_tag(self, user_id, tag_id):
        """
            Delete tags from data base.

            Args:
                user_id -- id of user performing the operation
                tag_id -- id of tag aim to be deleted

        """
        self.storage.delete_tag(tag_id)

    @write_debug_log
    @write_error_log
    @check_user_existence
    def update_tag(self, user_id, tag_id, params):
        """
            Update tags in data base.

            Args:
                user_id -- id of user performing the operation
                tag_id -- id of tag aim to be updated
                params -- dict of properties of tag for updating:
                          "property" (key): new_value (value)
        """
        tag = self.storage.get_tag(tag_id)
        if 'value' in params:
            tag.value = params['value']

        if 'default' in params:
            tag.default = True

        return self.storage.change_tag(tag)

