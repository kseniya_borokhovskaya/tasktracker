from django import forms

from .models import (
    Task,
    Priority,
    Tag,
    Type,
    Status
)


class NewTaskForm(forms.ModelForm):
    description = forms.CharField(
        widget=forms.Textarea(
            attrs={'rows': 5, 'placeholder': 'Deteils of the task'}
        ),
        max_length=1000,
        help_text='The max length of the text is 1000.'
    )

    date = forms.CharField(label='Deadline', widget=forms.DateTimeInput(attrs={'type': 'datetime'}), required=False)

    class Meta:
        model = Task
        fields = ['name', 'description', 'owner', 'date', 'status', 'priority', 'type', 'tags', 'place', ]


class PriorityForm(forms.ModelForm):
    class Meta:
        model = Priority
        fields = ['value', ]


class StatusForm(forms.ModelForm):
    class Meta:
        model = Status
        fields = ['value', ]


class TypeForm(forms.ModelForm):
    class Meta:
        model = Type
        fields = ['value', ]


class TagForm(forms.ModelForm):
    class Meta:
        model = Tag
        fields = ['value', ]