from django.db import models
# from django.contrib.postgres.fields import ArrayField
from django.contrib.auth.models import User


class Task(models.Model):
    name = models.CharField(max_length=150)

    owner = models.ForeignKey(User, related_name="owned_tasks")
    assignee = models.ForeignKey(User, related_name="tasks", null=True)

    type = models.ForeignKey("Type", related_name="+", null=True, blank=True, on_delete=models.SET_NULL)
    status = models.ForeignKey("Status", related_name="+", null=True, blank=True, on_delete=models.SET_NULL)
    priority = models.ForeignKey("Priority", related_name="+", null=True, blank=True, on_delete=models.SET_NULL)

    creation_date = models.DateTimeField(auto_now_add=True)
    date = models.DateTimeField(null=True, blank=True)
    start_time = models.DateTimeField(null=True, blank=True)
    finish_time = models.DateTimeField(null=True, blank=True)

    plan = models.ForeignKey("Plan", related_name="+", null=True, on_delete=models.SET_NULL)
    reminder = models.ForeignKey("Reminder", related_name="+", null=True, on_delete=models.SET_NULL)

    ancestor = models.ForeignKey('Task', null=True, on_delete=models.SET_NULL)
    blocking_tasks = models.ManyToManyField('Task', related_name='blocked_tasks')

    description = models.TextField(max_length=1000, blank=True)
    tags = models.ManyToManyField("Tag", related_name="+", blank=True)
    place = models.CharField(max_length=100, blank=True)


class Type(models.Model):
    value = models.CharField(max_length=150)
    default = models.NullBooleanField()


class Status(models.Model):
    value = models.CharField(max_length=150)
    default = models.NullBooleanField()


class Priority(models.Model):
    value = models.CharField(max_length=150)
    default = models.NullBooleanField()


class Tag(models.Model):
    value = models.CharField(max_length=150)
    default = models.NullBooleanField()


class Plan(models.Model):
    times = models.ManyToManyField('Time', blank=True)

    start_date = models.DateTimeField(null=True, blank=True)
    finish_date = models.DateTimeField(null=True, blank=True)
    repetition_start_date = models.DateTimeField(null=True, blank=True)
    repetition_finish_date = models.DateTimeField(null=True, blank=True)

    repetitions = models.ManyToManyField('Kind', blank=True)


class Reminder(models.Model):
    date = models.DateTimeField(null=True, blank=True)

    is_on_begin_time = models.BooleanField()
    is_on_finish_time = models.BooleanField()

    reminders = models.ManyToManyField('Kind', blank=True)


class Time(models.Model):
    date = models.DateTimeField(null=True, blank=True)
    start_time = models.DateTimeField(null=True, blank=True)
    finish_time = models.DateTimeField(null=True, blank=True)


class Kind(models.Model):
    number_of_periods = models.PositiveIntegerField(null=True, blank=True)
    days_of_week = models.PositiveIntegerField(null=True, blank=True)
    year = models.PositiveIntegerField(null=True, blank=True)
    month = models.PositiveIntegerField(null=True, blank=True)
    week = models.PositiveIntegerField(null=True, blank=True)
    day = models.PositiveIntegerField(null=True, blank=True)

