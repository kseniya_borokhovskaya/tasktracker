from django.shortcuts import render, get_object_or_404
from django.contrib.auth import login, authenticate
from django.contrib.auth.forms import UserCreationForm
from django.shortcuts import redirect
from .models import (
    Task,
    Priority,
    Type,
    Status,
    Tag
)
from .forms import (
    NewTaskForm,
    PriorityForm,
    TypeForm,
    StatusForm,
    TagForm
)
from django.contrib.auth.decorators import login_required
from django.views.generic import (
    CreateView,
    UpdateView,
    DeleteView,
    DetailView
)
from django.urls import reverse_lazy


def signup(request):
    if request.method == 'POST':
        form = UserCreationForm(request.POST)
        if form.is_valid():
            form.save()
            username = form.cleaned_data.get('username')
            raw_password = form.cleaned_data.get('password1')
            user = authenticate(username=username, password=raw_password)
            login(request, user)
            return redirect('home')
    else:
        form = UserCreationForm()
    return render(request, 'signup.html', {'form': form})


# @login_required('registration/login.html')
def home(request):
    tasks = Task.objects.all()
    return render(request, 'home.html', {'tasks': tasks})


# @login_required('registration/login.html')
def task_view(request, pk):
    task = get_object_or_404(Task, pk=pk)
    return render(request, 'task_view.html', {'task': task})


class CreateTaskView(CreateView):
    model = Task
    form_class = NewTaskForm
    success_url = reverse_lazy('home')
    template_name = 'task_create.html'


class UpdateTaskView(UpdateView):
    model = Task
    fields = ['name', 'description', 'date', 'place', ]
    success_url=reverse_lazy('home')
    template_name = 'task_update.html'


class DeleteTaskView(DeleteView):
    model=Task
    success_url=reverse_lazy('home')
    template_name='task_confirm_deletion.html'


class CreatePriorityView(CreateView):
    model = Task
    form_class = PriorityForm
    success_url = reverse_lazy('home')
    template_name = 'task_view.html'

