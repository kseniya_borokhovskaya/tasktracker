from django.apps import AppConfig


class BorksTrackerConfig(AppConfig):
    name = 'borks_tracker'
