from django.core.urlresolvers import reverse
from django.urls import resolve
from django.test import TestCase
from .views import home
from .models import Task


class HomeTests(TestCase):
    def setUp(self):
        self.task = Task.objects.create(name='Do lab', description='Very important')
        url = reverse('home')
        self.response = self.client.get(url)

    def test_home_view_status_code(self):
        self.assertEquals(self.response.status_code, 200)

    def test_home_url_resolves_home_view(self):
        view=resolve('/')
        self.assertEquals(view.func, home)

